


class PrimeNum{
	
	static int cnt = 0;

	static boolean checkPrime(int num,int temp){
		
		if(cnt>2 && num==temp) return false;

		if(cnt<=2 && num==temp) return true;

		return checkPrime(num,temp-1);
		
	}

	public static void main(String[] args){
	
		boolean num = checkPrime(13,1);

		System.out.println(num);
	}
}
