




class CheckPallindrome{

	static int rev = 0;

	static int reverseNum(int num){
	
		if(num/10==0) return (rev*10) + (num%10);

		rev = rev * 10 + (num%10);

		return reverseNum(num/10);
	}

	public static void main(String[] args){
		
		int num = 123217;
		int reverse = reverseNum(num);

		if(num == reverse)
			System.out.println("Number is Pallindrome");
		else
			System.out.println("Number is not Pallindrome");

	}
}
