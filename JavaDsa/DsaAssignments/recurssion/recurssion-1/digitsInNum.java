


class digitsCount{
	
	static int count = 0;
	static int countDigit(int num){
		
		if(num/10==0) return 1;

		return 1 + countDigit(num/10);
		
	}

	public static void main(String[] args){
	
		System.out.println(countDigit(12345678));
	}
}
