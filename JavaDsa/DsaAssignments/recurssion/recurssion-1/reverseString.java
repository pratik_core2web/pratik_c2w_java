




class StringReverse{
	
	static String revStr = "";
	static int i=0;
	static String reverse(String str){
		
		if(i==str.length()){
			return revStr; 
		}
		revStr=str.charAt(i)+revStr;
		i++;
		return reverse(str);
		
	}

	public static void main(String[] args){
	
		String str = "Pratik";

		String revString = reverse(str);

		System.out.println(revString);
	}
}
