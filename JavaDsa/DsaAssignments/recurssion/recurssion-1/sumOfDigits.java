



class SumOfDigits{
	
	static int cnt = 0;

	static int sumCalculate(int num){
		
		if(num/10==0) return num;

		return (num%10) + sumCalculate(num/10);
	}

	public static void main(String[] args){
	
		int sum = sumCalculate(12345);

		System.out.println(sum);
	}
}
