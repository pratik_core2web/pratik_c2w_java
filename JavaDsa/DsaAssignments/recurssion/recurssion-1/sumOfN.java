

class sumOfN{
	
	static int printSum(int num){
	
		if(num==0) return 0;

		return num + printSum(num-1);

	}
	public static void main(String[] args){
		
		System.out.println(printSum(10));
	}
}
