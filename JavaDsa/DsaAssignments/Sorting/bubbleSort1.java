import java.util.*;


class BubbleSort{
	
	static void bubbleSort(int arr[], int n){
	
		boolean flag = false;

		if(n==1)
			return;

		else{
		
			for(int j = 0; j<arr.length-1; j++){
			
				if(arr[j]>arr[j+1]){
				
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
					flag = true;
				}
			}
			if(flag == false)
				return;

			bubbleSort(arr,n-1);
		}
	}

	/*static void bubbleSort(int arr[], int start, int end){
	
		boolean flag = false;

		for(int i = 0; i<arr.length; i++){
			
			flag = false;

			for(int j = 0; j<arr.length-1-i; j++){
			
				if(arr[j] > arr[j+1]){
				
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
					flag = true;
				}
			}

			if(flag == false) break;
		}
	}*/

	public static void main(String [] args){
	
		int arr[] = {4,1,3,9,7};

		System.out.println(Arrays.toString(arr));

		bubbleSort(arr,arr.length);

		System.out.println(Arrays.toString(arr));

	}
}
