

import java.util.*;

class SelectionSort{

	static void selectionSort(int arr[]){
	
		for(int i= 0; i<arr.length; i++){
		
			int minIdx = i;

			for(int j = i+1; j<arr.length; j++){
			
				if(arr[j] < arr[minIdx]){
				
					minIdx = j;
				}
			}

			int temp = arr[i];
			arr[i] = arr[minIdx];
			arr[minIdx]=temp;
		}
	}

	public static void main(String[] args){
	
		int arr[] = {0,2,1,2,0};

		System.out.println("Before sorting : "+ Arrays.toString(arr));

		selectionSort(arr);

		System.out.println("After sorting : "+ Arrays.toString(arr));
	}
}
