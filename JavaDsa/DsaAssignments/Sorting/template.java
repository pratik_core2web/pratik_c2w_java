
import java.util.*;

class SelectionSort{

	public static void main(String[] args){
	
		int arr[] = {0,2,1,2,0};

		System.out.println("Before sorting : "+ Arrays.toString(arr));

		selectionSort(arr);

		System.out.println("After sorting : "+ Arrays.toString(arr));
	}
}
