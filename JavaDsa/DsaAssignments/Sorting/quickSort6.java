
import java.util.*;

class QuickSort{
	
	static int partition(int arr[], int start, int end){
	
		int pivotElement = arr[end];

		int i = -1;

		for(int j = 0; j<end; j++){
		
			if(arr[j] < pivotElement){
			
				i++;

				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}

		}
		int temp = arr[i+1];
		arr[i+1] = arr[end];
		arr[end] = temp;

		return i+1;
	}

	static void quickSort(int arr[], int start,int end){
	
		if(start < end){
		
			int pivotIndex = partition(arr,start,end);
			
			System.out.println(pivotIndex);

			quickSort(arr,start,pivotIndex-1);

			quickSort(arr,pivotIndex+1,end);

		}
	}

	public static void main(String[] args){
	
		int arr[] = {1,2,9,4,69,6,5};

		int start = 0;
		int end = arr.length-1;

		System.out.println("Before sorting : "+ Arrays.toString(arr));

		quickSort(arr,start,end);

		System.out.println("After sorting : "+ Arrays.toString(arr));
	}
}
