
import java.util.*;

class InsertionSort{

	static void insert(int arr[], int i){
	
		if(i==arr.length)  return;

		else{
		
			int element = arr[i];

			int j = i-1;

			while(j>=0 && arr[j]>element){
			
				arr[j+1] = arr[j];
				j--;
			}

			arr[j+1] = element;
			insert(arr,i+1);	
		}
	}

	static void insertionSort(int arr[]){
	
		for(int i = 1; i<arr.length; i++){
		
			int element = arr[i];

			int j = i-1;

			while(j>=0 && arr[j]>element){
			
				arr[j+1] = arr[j];
				j--;
			}

			arr[j+1] = element;
			
		}
	}
	
	public static void main(String [] args){
	
		int arr[] = {0,2,1,2,0};

		System.out.println(Arrays.toString(arr));

		insert(arr,1);

		System.out.println(Arrays.toString(arr));

	}
}
