
import java.util.*;

class MergeArrays{

	static int[] merge(int arr1[],int arr2[], int m, int n){
	
		int arr[] = new int[m+n];

		int i = 0, j = 0 , k=0;

		while(i<m && j<n){
			
			if(arr1[i]<arr2[j]){
			
				arr[k++] = arr1[i++];
			}else{
			
				arr[k++] = arr2[j++];
			}
		}

		while(i<m){
		
			arr[k++] = arr1[i++];
		}
		
		while(j<n){
		
			arr[k++] = arr2[j++];
		}

		return arr;
	}

	public static void main(String[] args){
	
		int m = 4;
		int n = 3;

		int arr1[] = new int[]{3,8,12,18,0,0,0};

		int arr2[] = new int[]{1,5,35};

		int arr[] = merge(arr1,arr2,m,n);

		System.out.println(Arrays.toString(arr));
	}
}
