
import java.util.*;

class SelectionSort{

	static boolean isSorted(int arr[]){
	
		for(int i= 0; i<arr.length-1; i++){
		
			if(arr[i] > arr[i+1])
				return false;
		}
		return true;
	}

	public static void main(String[] args){
	
		int arr[] = {10,20,5000,40};
		
		if(isSorted(arr)){
			System.out.println("Array is Sorted");
		}else
			System.out.println("Array is not Sorted");
	}
}
