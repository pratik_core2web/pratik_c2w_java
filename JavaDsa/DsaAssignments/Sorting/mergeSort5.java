

//Given an array arr[] sort it using merge sor

import java.util.*;

class Mergesort{
	
	static void merge2Arrays(int arr[],int start,int mid,int end){
			
		int n1 = mid-start+1;

		int n2 = end-mid;

		int arr1[] = new int[n1];

		int arr2[] = new int[n2];

		for(int i = 0; i<arr1.length; i++){
		
			arr1[i] = arr[start+i];
		}

		for(int j = 0; j< n2; j++){
		
			arr2[j]=arr[mid+1+j];
		}

		int i = 0;
		int j= 0;
		int k = start;

		while(i<n1 && j<n2){
		
			if(arr1[i]<arr2[j]){
				
				arr[k]=arr1[i];
				i++;
			}else{
			
				arr[k] = arr2[j];
				j++;
			}
			k++;
		}

		while(i<n1){
		
			arr[k]=arr1[i];
			k++;
			i++;
		}

		while(j<n2){
		
			arr[k] = arr2[j];
			j++;
			k++;
		}
	}

	static void mergeSort(int arr[], int start, int end){
	
		if(start<end){
		
			int mid = start + (end-start)/2;

			mergeSort(arr,start,mid);

			mergeSort(arr,mid+1,end);

			merge2Arrays(arr,start,mid,end);
		}
	}

	public static void main(String[] args){
	
		int arr[] = {9,5,10,3,2,7};

		System.out.println(Arrays.toString(arr));

		mergeSort(arr,0,arr.length-1);
		
		System.out.println(Arrays.toString(arr));
	}
}
