

import java.util.*;

class SelectionSort{

	static void mergeArray(int arr[], int arr1[], int arr2[]){
	
		int i = 0;
		int j = 0;
		int k = 0;

		while(i<arr1.length && j<arr2.length){
		
			if(arr1[i] < arr2[j]){
			
				arr[k] = arr1[i];
				i++;
			}else{
			
				arr[k] = arr2[j];
				j++;
			}
			k++;
		}

		while(i<arr1.length){
			
			arr[k] = arr1[i];
			k++;
		       	i++;
		}

		while(j<arr2.length){
			
			arr[k] = arr2[j];
			k++;
		       	j++;
		}
	}

	public static void main(String[] args){
	
		int arr1[] = {0,2,8};

		int arr2[] = {1,5,6,7,8,9,12};
		
		int arr[] = new int[arr1.length+arr2.length];

		mergeArray(arr,arr1,arr2);

		System.out.println("After sorting : "+ Arrays.toString(arr));
	}
}
