



class MaxSubOfK{

	public  static void main(String []args){

		int arr[] = {1,2,3,1,4,5,2,3,6};

		int k = 3;
		int n = arr.length;

		int num = Integer.MIN_VALUE;
		int maxNum = Integer.MIN_VALUE;

		for(int i = 0; i<n-k+1; i++){
			
			for(int j = i; j<k+i; j++){
				
				num = arr[j];

				if(num>maxNum)
					maxNum = num;

			}
			System.out.print(maxNum+"\t");

		}
		System.out.println();
		
	}
}

