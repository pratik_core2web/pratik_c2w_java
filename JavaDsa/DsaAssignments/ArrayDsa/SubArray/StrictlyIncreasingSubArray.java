


class SubArraySum{
	
	public static void main(String [] args){

		int arr[] = {1,2,3,2,5,1,7};

		int sum = arr[0];
		int maxSum = arr[0];

		for(int i = 1; i<arr.length; i++){
		
			if(arr[i-1] < arr[i]){

				sum = sum + arr[i];

				if(sum>maxSum)
					maxSum = sum;
			}

			else{
				
				sum = arr[i];
			}
		}
		System.out.println(maxSum);
	}
}
