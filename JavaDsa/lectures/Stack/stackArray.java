
import java.util.*;
import java.io.*;

class StackArray {

	int arr[];
	int top = -1;
	int maxSize;

	StackArray(int size){
	
		this.maxSize = size;
		this.arr = new int[size];
	}

	void push(int data){
		
		if(top == maxSize-1){
			
			System.out.println("Stack is Full");
			return;
		}	
		else{
		
			top++;
			arr[top] = data;
		}
	
	}

	int pop(){
		
		if( empty() ){
		
			System.out.println("Stack is empty");
			return -1;
		}else{
		
			int ret = arr[top];
			top--;
			return ret;
		}
	}

	boolean empty(){
	
		if(top == -1)
			return true;

		else
			return false;
	}

	int peek(){
	
		if(empty()){
		
			System.out.println("Stack is empty");
			return -1;
		}else{
		
			return arr[top];
		}
	}

	int size(){
	
		return top;
	}

	void printStack(){
		
		if(empty()){

			System.out.println("Stack is empty");
		}

		else{
			System.out.print("[");

			for(int i = 0; i<=top; i++){
				
				if(i==top){
					
					System.out.print(arr[i]);
				}
				else{
					System.out.print(arr[i]+"\t");
		
				}
			}

			System.out.println("]");
		}
	}
}

class Client{

	public static void main(String... args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));	

		System.out.print("\nEnter the size of stack : ");

		int size = Integer.parseInt(br.readLine());

		StackArray st = new StackArray(size);

		char ch;

		do{
			System.out.println("\n*************** Select From Options ***************\n");

			System.out.println("1.Push");
			System.out.println("2.Pop");
			System.out.println("3.Peek");
			System.out.println("4.Empty");
			System.out.println("5.Size");
			System.out.println("6.Print Stack\n");


			System.out.print("Enter your choice : ");

			int count = Integer.parseInt(br.readLine());

			switch(count){
				
				case 1 : 
					{
						System.out.print("\nEnter the data  : ");
						int data = Integer.parseInt(br.readLine());

						st.push(data);
					}
					break;

				case 2:
					{
						int ret = st.pop();

						if(ret != -1){
				
							System.out.println('\n'+ret+" Popped");			
						}
					}
					break;

				case 4:
					{
						if(st.empty()){
							
							System.out.println("\nStack is Empty");
						}
						else{
						
							System.out.println("\nStack is not Empty");
						}
					
					}
					break;

				case 5:
					{
						int stackSize = st.size();

						System.out.println("\nSize of Stack : "+stackSize);
					
					}
					break;

				case 3:
					{
						int ret = st.peek();

						if(ret != -1){
						
							System.out.println("\nPeek of Stack : "+ret);
						}
					
					}
					break;

				case 6:
					{
						st.printStack();
					
					}
					break;

				default:
					{
					
						System.out.println("\nWrong choice..!");
					}
			}

			System.out.print("\nDo you want to continue..? : ");

			String str = br.readLine();

			ch = str.charAt(0);
			
		}while(ch=='y'||ch=='Y');
	}
}
