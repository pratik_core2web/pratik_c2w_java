


class Recursion4{
	
	static void fun(int num){

		if(num==10)
			return;

		fun(++num);
		System.out.println(num);

	}

	public static void main(String [] args){
	
		fun(0);
	
	}

}

