


import java.util.*;

class BinarySearchReccur{


	int count = 0;

	int binarySearch(int arr[],int start, int end, int target){
		
		int mid = (start+end)/2;

		//base condition

		if(start>end){
			return -1;
		}

		else{
			count++;

			if(arr[mid]==target){
				
				System.out.println(count);
				return mid;
			}
			if(arr[mid]<target)
				return binarySearch(arr,mid+1,end,target);

			else
				return binarySearch(arr,start,mid-1,target);
			
		}

	}

	public static void main(String[] args){
	
		int arr[]=new int[]{10,20,30,40,55,63,79,89,99,113};

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the Element to be search : ");

		int element = sc.nextInt();

		BinarySearchReccur obj = new BinarySearchReccur();

		int index = obj.binarySearch(arr,0,arr.length-1,element);

		if(index == -1){
		
			System.out.println("Element not found in an array.");
		
		}else{
		
			System.out.println("Element Found at Index : "+ index);
		}
		

		System.out.println("********************");
		System.out.println("Count"+ obj.count);
	}
}
