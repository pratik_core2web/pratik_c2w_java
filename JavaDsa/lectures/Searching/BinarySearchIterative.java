


import java.util.*;

class BinarySearch{
	
	int binarySearch(int arr[],int target){

		int start = 0;
		int end = arr.length-1;

		while(start<=end){
		
			int mid = (start+end)/2;

			if(arr[mid]==target)
				return mid;

			else if(arr[mid]>target)
				end = mid-1;
			
			else
				start = mid+1;  //arr[mid]<target   50<100
		}

		return -1;	

	}

	public static void main(String[] args){
	
		int arr[]=new int[]{10,20,30,40,55,63,79,89,99,113};

		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the Element to be search : ");

		int element = sc.nextInt();

		BinarySearch obj = new BinarySearch();

		int index = obj.binarySearch(arr,element);

		if(index == -1){
		
			System.out.println("Element not found in an array.");
		
		}else{
		
			System.out.println("Element Found at Index : "+ index);
		}
	}
}
