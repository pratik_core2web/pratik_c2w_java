



class EquiIndOptimized {
	
	static void prefix(int arr[]){
		
		int leftSum[] = new int[arr.length];
		int rightSum[] = new int[arr.length];
	
		int n = arr.length;
		int flag = 0;

		for(int i = 0; i<n; i++){
		
			if(i==0)
				leftSum[i]=arr[i];

			else
				leftSum[i] = leftSum[i-1] + arr[i];
		}

		for(int i = n-1; i>=0;i--){
		
			if(i<=n-2)
				rightSum[i] = rightSum[i+1] + arr[i];

			else 
				rightSum[i] = arr[i];
		}

		for(int i = 0; i<n; i++){
		
			if(leftSum[i]==rightSum[i]){
			
				System.out.println(i);
				flag=1;
				break;
			}
		}

		if(flag==0)
			System.out.println("-1");
	}

	public static void main(String[] args){
	
		int arr[] = new int[] {-7,1,5,2,-4,3,0};

		prefix(arr);
	}
}
