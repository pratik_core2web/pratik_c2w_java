


class SubArray{

	public static void main(String[] args){
	
		int arr[] = new int[] {1,2,3,1,3,4,6,4,6,3};
		
		int n = arr.length;
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;

		for(int i = 0; i<n; i++){
		
			if(max<arr[i])
				max=arr[i];

			if(min>arr[i])
				min = arr[i];
		}
		
		int length = 0;
		int minLength = Integer.MAX_VALUE;

		for(int i =0; i<n; i++){

			if(arr[i]==min){

				for(int j = i+1; j<n;j++){
				
					if(arr[j]==max){
					
						length = j-i+1;

						if(length<minLength){
							minLength = length;
						}
					}
				}
			}

		       else if(arr[i]==max){

				for(int j = i+1; j<n;j++){
				
					if(arr[j]==min){
					
						length = j-i+1;

						if(length<minLength){
							minLength = length;
						}
					}
				}
		       }
		}
		System.out.println(minLength);
	}
}
