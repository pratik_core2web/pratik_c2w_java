
/*Given a charv array(lowercase)
 * Return count of pair(i,j,k) such that
 *
 * a) i<j<k
 *
 * b) arr[i]= 'a'
 *    arr[j]= 'b'
 *    arr[k]= 'c'
 *
 * c) arr[a,b,c,a,b,c]
 *
 * output = 3*/



class PairOf3 {

	static int pairCount(int arr[]){
	
		int cnt = 0;

		for(int i = 0; i<arr.length; i++){
		
			if(arr[i]=='a'){
			
				for(int j = i+1; j<arr.length; j++){

					if(arr[j]=='b'){

						for(int k = j+1; k<arr.length; k++){
						
							if(arr[k]=='c')
								cnt++;

						}
					}
				}
			}
				
		}
		return cnt;
	}

	public static void main(String[] args){
	
		int arr[] = new int[]{'a','b','c','a','b','c'};

		int pairs = pairCount(arr);

		System.out.println(pairs);

	}
}
