
/*Given a charv array(lowercase)
 * Return count of pair(i,j) such that
 *
 * a) i<j
 * b) arr[i]= 'a'
 *    arr[j]= 'g'
 *
 * c) arr[a,b,e,g,a,g]
 *
 * output = 3*/

class PairCount{

	
	 int pairC(int arr[]){
	
		int count = 0;	
		for(int i = 0; i<arr.length; i++){
		
			if(arr[i]=='a'){
				
				for(int j = i+1; j<arr.length; j++){

					if(arr[j]=='g')
						count++;
				}
			}
		}
		return count;
	}
	

	public static void main(String [] args){
		
		PairCount pc = new PairCount();
			
		int arr[]=new int[]{'a','g','a','g','a','g'};

		System.out.println(pc.pairC(arr));
	}
}
