

//Optimized way


class PairC{

	static int pairCount(int arr[]){
	
		int count = 0;
		int pair = 0;
		
		for(int i = 0; i<arr.length; i++){
	
			if(arr[i]=='a')
				count++;

			else if(arr[i]=='g')
				pair = pair + count;

		}

		return pair;

	}

	public static void main(String[] args){
	
		int arr[] = new int[]{'a','g','a','g','a','g'};

		int pair = pairCount(arr);

		System.out.println(pair);

	}
}
