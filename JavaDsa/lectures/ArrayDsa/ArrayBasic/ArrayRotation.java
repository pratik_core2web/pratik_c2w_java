




class ArrayRotation{
	
	static void Rotate(int[] arr1,int N,int d){
	
		int arr2[] = new int[N];
		int k = 0;

		for(int i = d; i<arr2.length; i++){
			arr2[k]=arr1[i];
			k++;
		}

		for(int i=0; i<d; i++){
			arr2[k]=arr1[i];
			k++;
		}

		for(int i=0; i<N; i++){
			arr1[i]=arr2[i];
		}	
	}

	public static void main(String[] args){

		int arr1[] = new int[]{1,2,3,4,5,6,7,8,9};
		int N = arr1.length;
		int d = 3;

		Rotate(arr1, N, d);

		for(int i = 0; i<N;i++){
			System.out.print(arr1[i]+"\t");
		}
		System.out.println();

	}
}
