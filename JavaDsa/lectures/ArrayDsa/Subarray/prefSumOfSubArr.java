



class SumOfSubarry{

	public static void main(String[] args){
	
		int arr[] = new int[]{2,4,1,3};

		int pSum[] = new int[arr.length];

		pSum[0]=arr[0];

		for(int i = 1; i<arr.length; i++){
		
			pSum[i]=pSum[i-1]+arr[i];
		}

		for(int i = 0; i<arr.length;i++){

			for(int j = i; j<arr.length; j++){
			
				int sum = 0;
				
				if(i==0)
					sum = pSum[j];
				else
					sum = pSum[j]-pSum[i-1];

				System.out.println(sum);
			}
		}
	}
}
