



class PrefMaxSubArrays{

	public static void main(String[] args){
	
		int arr[] = {-3,4,-2,5,3,-2,8,2,1,4};
		int k = 4;

		int pSum[] = new int[arr.length];
		
		pSum[0] = arr[0];

		for(int i = 1; i<arr.length; i++){

			pSum[i] = pSum[i-1]+arr[i];
		}

		int sum = 0;
		int maxSum = sum;

		int start = 0;
		int end = k-1;

		while(end<arr.length){

			if(start == 0)
				sum = pSum[end];

			else
				sum = pSum[end]-pSum[start-1];
				
			if(sum>maxSum)
				maxSum = sum;
		
			start++;
			end++;
			
		}
		System.out.println("Maxsum of subarray : " +maxSum);
	}
}
