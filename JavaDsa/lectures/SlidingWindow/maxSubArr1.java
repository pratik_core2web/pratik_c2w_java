



class MaxSubArrays{

	public static void main(String[] args){
	
		int arr[] = {-3,4,-2,5,3,-2,8,2,1,4};
		int k = 4;

		int end = k-1;
		int start = 0;

		int maxSum = 0;

		while(end<arr.length){
		
			int sum = 0;

			for(int i = start; i<=end; i++){
			
				sum = sum + arr[i];
			}
				
			if(sum>maxSum)
				maxSum = sum;
		
			start++;
			end++;
			
		}
		System.out.println("Maxsum of subarray : " +maxSum);
	}
}
