


class BubbleReccursion{

	void bubblesort(int arr[],int n){
	
		if(n==1)
			return;

		boolean swapped = false;

		for(int j = 0; j<n-1; j++){
			if(arr[j]>arr[j+1]){
			
				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
				swapped = true;
			}
		}

		if(swapped == false)
			return;

		bubblesort(arr,n-1);
		System.out.println(n);
	}

	public static void main(String[] args){
	
		int arr[] = {98,23,56,74,52};

		BubbleReccursion obj = new BubbleReccursion();

		obj.bubblesort(arr,arr.length);

	//	for(int i = 0; i<arr.length;i++){
	//		System.out.println(arr[i]);
	//	}
	}
}
