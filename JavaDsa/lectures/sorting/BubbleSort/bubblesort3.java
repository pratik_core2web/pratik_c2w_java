



class BubbleSort{

	int count = 0;
	boolean flag = false; 

	void bubbleSort(int arr[]){

		for(int i = 0; i<arr.length; i++){
			
			flag = false;

			for(int j = 0; j<arr.length-1-i; j++){
			
				count++;
				if(arr[j]>arr[j+1]){
				
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
					flag = true;
				}
			}
			if(flag==false)
				break;
		}

		for(int i = 0; i<arr.length; i++){
			System.out.println(arr[i]);
		}

		System.out.println("Count is : "+count);
		
	}

	public static void main(String[] args){
	
		int arr[] = {1,2,3,4,5,6,7};
		
		BubbleSort obj = new BubbleSort();
		obj.bubbleSort(arr);

	}
}
