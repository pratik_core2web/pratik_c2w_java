

import java.util.Arrays;

class QuickSort{

	static int partition(int arr[],int start,int end){
	
		int pivotElement = arr[end];  //when pivot index is last element

		int i = start-1;

		for(int j = start; j<end; j++){
		
			if(arr[j] <= pivotElement){
			
				i++;

				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}

		int temp = arr[i+1];
		arr[i+1] = arr[end];   //element at pivot element i.e err[end];
		arr[end] = temp;

		return i+1;
	}

	static void quickSort(int arr[], int start, int end){
		
		if(start<end){
		
			int pivotIndex = partition(arr,start,end);  //element at pivot index is at its proper position

			quickSort(arr, start, pivotIndex-1);

			quickSort(arr, pivotIndex+1, end);	
		}
	}

	public static void main(String[] args){
	
		int arr[] = {9,1,8,2,7,3,6,4,5};

		System.out.println(Arrays.toString(arr));
		
		int start = 0;

		int end = arr.length-1;

		quickSort(arr,start,end);
		
		System.out.println(Arrays.toString(arr));
	}
}
