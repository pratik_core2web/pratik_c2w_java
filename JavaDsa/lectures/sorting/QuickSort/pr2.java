

import java.util.Arrays;

class QuickSortStart{

	static int partition(int arr[],int start,int end){
	
		int pivotElement = arr[start];  //when pivot index is start element

		int i = end+1;

		for(int j = end; j>start; j--){
		
			if(arr[j] >= pivotElement){
			
				i--;

				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
		int temp = arr[i-1];
		arr[i-1] = arr[start];  //the element in pivotIndex i.e arr[start];
		arr[start] = temp;

		return i-1;
	}

	static void quickSort(int arr[], int start, int end){
		
		if(start<end){
		
			int pivotIndex = partition(arr,start,end);  //element at pivot index is at its proper position

			quickSort(arr, start, pivotIndex-1);

			quickSort(arr, pivotIndex+1, end);	
		}
	}

	public static void main(String[] args){
	
		int arr[] = {9,1,8,2,7,3,6,4,5};

		System.out.println(Arrays.toString(arr));
		
		int start = 0;

		int end = arr.length-1;

		quickSort(arr,start,end);
		
		System.out.println(Arrays.toString(arr));
	}
}
