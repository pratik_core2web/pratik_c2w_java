

class RecurrsionSelect{

	static void selectionSort(int arr[], int i){
	
		if(i==arr.length-1){
			return;
		}

		else{
			int mdx = i;

			for(int j = i+1; j<arr.length; j++){
			
				if(arr[j]< arr[mdx]){
				
					mdx = j;
				}
			}

			int temp = arr[i];
			arr[i] = arr[mdx];
			arr[mdx] = temp;

			selectionSort(arr,i+1);
		}
	}

	public static void main(String[] args){
	
		int arr[] = {5,8,9,7,53,1,-2,3};

		selectionSort(arr,0);

		for(int i = 0; i<arr.length; i++){
		
			System.out.print(arr[i]+"\t");
		}
		System.out.println();
	}
}
