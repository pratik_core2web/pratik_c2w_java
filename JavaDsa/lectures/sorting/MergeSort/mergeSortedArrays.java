


class MergeSortedArrays{

	public static void main(String[] args){
	
		int arr1[] = {5,9,11,13};

		int arr2[] = {1,8,17};

		int arr3[] = new int[arr1.length + arr2.length];


		int i = 0;
		int j = 0;
		int k = 0;

		while(i<arr1.length && j<arr2.length){
		
			if(arr1[i] < arr2[j]){
			
				arr3[k] = arr1[i];
				i++;
			}else{
			
				arr3[k] = arr2[j];
				j++;
			}

			k++;
		}

		while(i<arr1.length){
		
			arr3[k] = arr1[i];
			k++;
			i++;
		
		}
		while(j<arr2.length){
		
			arr3[k] = arr2[j];
			k++;
			j++;
		}

		for(int x = 0; x < arr3.length; x++){
		
			System.out.print(arr3[x]+"\t");
		}

		System.out.println();
	}
}
