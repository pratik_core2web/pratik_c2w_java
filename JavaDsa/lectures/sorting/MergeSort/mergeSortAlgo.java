

class MergeSortAlgo{

	void mergeArrays(int arr[],int start,int mid,int end){

		int n1 = mid-start+1;

		int n2 = end - mid;

		int arr1[] = new int[n1];
		
		int arr2[] = new int[n2];

		for(int i = 0; i<n1 ; i++){
		
			arr1[i] = arr[start + i];
		}
	
		for(int j = 0; j<n2 ; j++){
		
			arr2[j] = arr[mid + 1 + j];
		}

		int i = 0;

		int j = 0;

		int k = start; //for original array filling it sometimes 0 or sometimes any other number

		while(i<arr1.length && j<arr2.length){
		
			if(arr1[i] < arr2[j]){
			
				arr[k] = arr1[i];
				i++;
			}else{
			
				arr[k] = arr2[j];
				j++;
			}
			k++;
		}

		while(i<arr1.length){

			arr[k] = arr1[i];
			i++;
			k++;		
		}
	
		while(j<arr2.length){

			arr[k] = arr2[j];
			j++;
			k++;		
		}
	}

	void divideArray(int arr[],int start, int end){
	
		if(start < end){
		
			int mid = start + (end-start)/2;
			
			divideArray(arr,start,mid);  //1st half of array
			
			divideArray(arr,mid+1,end);  //2nd half of array

			mergeArrays(arr,start,mid,end); //mergeing sorted arrays  (now bottom to top approach)
		}
	}

	public static void main(String[] args){
	
		int arr[] = {8,7,6,5,1,3,9,4,2};

		int start = 0;
		int end = arr.length-1;

		MergeSortAlgo obj = new MergeSortAlgo();

		obj.divideArray(arr,start,end);

		System.out.print("After Sorting : ");
		
		for(int i = 0; i<arr.length;i++){
		
			System.out.print(arr[i]+"\t");
		}
	
		System.out.println();
	}
}
