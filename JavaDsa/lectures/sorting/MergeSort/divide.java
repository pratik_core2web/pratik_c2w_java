


class Divide{

	void divideArray(int arr[], int start, int end){
	
		if(start < end){
		
			int mid = start + (end - start)/2;

			System.out.println(start+"\t"+mid+"\t"+end);

			divideArray(arr,start,mid);
			divideArray(arr,mid+1,end);

		}
	}

	public static void main(String[] args){
	
		int arr[]=new int[]{5,2,9,3,4,1,7};

		int start = 0;

		int end = arr.length-1;

		Divide obj = new Divide();

		obj.divideArray(arr,start,end);
	}
}
