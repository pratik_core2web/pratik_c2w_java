



class InsertionSort{

	void insertionSort(int arr[]){
	
		for(int i = 1; i<arr.length; i++){
		
			int element = arr[i];
			int j = i-1;

			while(j>=0 && arr[j]>element){
			
				arr[j+1] = arr[j];
				j--;
			}

			arr[j+1] = element;
		}
	}
	public static void main(String[] args){
	
		int arr[] = {3,6,7,2,1};

		InsertionSort obj = new InsertionSort();

		obj.insertionSort(arr);

		for(int i = 0; i<arr.length; i++){
		
			System.out.println(arr[i]);
		}
	}
}
