



class RecurrsionInsert{
	
	void insertionSort(int arr[], int i){
	
		if(i==arr.length)
			return;

		int element = arr[i];

		int j = i-1;

		while(j>=0 && arr[j]>element){
		
			arr[j+1] = arr[j];
			j--;
		}
		arr[j+1]=element;

		insertionSort(arr,i+1);
	}

	public static void main(String[] args){
	
		int arr[] = {9,6,2,1,3,5,4,8,7};

		RecurrsionInsert obj =new RecurrsionInsert();

		obj.insertionSort(arr,1);

		for(int i =0;i<arr.length; i++){
		
			System.out.print(arr[i]+"\t");

		}

		System.out.println();
	}
}
