import java.util.*;

class Node{

	Node prev = null;
	int data ;
	Node next = null;

	Node(int data){
	
		this.data = data;
	}
}

class DLL{

	Node head = null;

	void addFirst(int data){
		
		Node newNode = new Node(data);

		if(head==null){
			
			head = newNode;	
		}else{
		
			newNode.next = head;
			head.prev = newNode;
			head = newNode;
		}
	
	}

	void addLast(int data){
		
		Node newNode = new Node(data);

		if(head == null){
			head = newNode;
		}else{
		
			Node temp = head;
			while(temp.next!=null){
				
				temp = temp.next;
			}
			temp.next = newNode;
			newNode.prev = temp;
		}
		
	}

	void addAtPos(int pos,int data){
	
		if(pos<=0 || pos>=countNode()+2){
		
			System.out.println("Wrong Input");
			return;
		}	
		if(pos==1){
			addFirst(data);

		}else if(pos==countNode()+1){
		
			addLast(data);
		}else{
			Node newNode = new Node(data);	
			Node temp =head;
			while(pos-2!=0){
				
				temp =temp.next;
				pos--;
			}
			newNode.next = temp.next;
			newNode.prev = temp;
			temp.next = newNode;
			newNode.next.prev = newNode;
		}
	}

	void delLast(){
	
		if(head==null){
			
			System.out.println("Wrong Input\n");
			return;
		}else if(head.next==null){
		
			head = null;
		}else{
		
			Node temp = head;
			while(temp.next != null){
			
				temp = temp.next;
			}

			temp.prev.next = null;
			temp.prev = null;
		}
	}

	void delFirst(){
		
		if(head==null){
			
			System.out.println("Wrong Input\n");
			return;
		}else if(head.next==null){
		
			head = null;
		}else{
		
			head = head.next;
			head.prev = null;
		}
	
	}

	void delAtPos(int pos){
		
		if(pos<=0||pos>=countNode()+1)	{
		
			System.out.println("Wrong Input\n");
			return;
		}
		if(pos==1){
		
			delFirst();

		}else if(pos==countNode()){
		
			delLast();
		}else{
		
			Node temp = head;

			while(pos-2!=0){
			
				pos--;
				temp = temp.next;
			}
			temp.next = temp.next.next;
			temp.next.prev = temp;
		}
	
	}

	int countNode(){	
		
		int count = 0;
		Node temp = head;

		while(temp!=null){
		
			count++;
			temp = temp.next;
		}
		return count;
	}

	void printDll(){
		
		if(head == null){
		
			System.out.println("\nEmpty Linked List.");
		}else{
		
			Node temp = head;

			while(temp.next!=null){
				
				System.out.print(temp.data+" -> ");
				temp = temp.next;
			}
			System.out.println(temp.data+"\n");
		}
	}
}

class Client{

	public static void main(String[] args){
	
		DLL obj = new DLL();

		Scanner sc = new Scanner(System.in);

		char ch;
		
		do{

			System.out.println("********* Choose from Options **********\n");
			System.out.println("1.Add First");
			System.out.println("2.Add Last");
			System.out.println("3.Add at Position");
			System.out.println("4.Delete First");
			System.out.println("5.Delete Last");
			System.out.println("6.Delete at Position");
			System.out.println("7.Count Nodes");
			System.out.println("8.Print DLL");

			System.out.print("\nSelect the Option : ");
			int option = sc.nextInt();

			switch(option){
			
				case 1: 
					{
						System.out.print("\nEnter the data: ");
						int data = sc.nextInt();
						obj.addFirst(data);
					}
					break;

				case 2 :
					{			
						System.out.print("\nEnter the data: ");
						int data = sc.nextInt();
						obj.addLast(data);
					}
					break;
				
				case 3 : 
	                          	{
						System.out.print("\nEnter the Position: ");
						int pos = sc.nextInt();
						System.out.print("\nEnter the data: ");
						int data = sc.nextInt();
						obj.addAtPos(pos,data);
					}
					break;

				case 4 :
					
					obj.delFirst();
					break;

				case 5 : 
					obj.delLast();
					break;	

				case 6 : 
					System.out.print("\nEnter the Position: ");
					int pos = sc.nextInt();
					obj.delAtPos(pos);
					break;

				case 7 : 
					int count = obj.countNode();
					System.out.println("Total Node : "+count+"\n");
					break;	

				case 8 : 
					obj.printDll();
					break;
						
				default:
					System.out.println("Wrong Input...!");

			}

			System.out.print("You want to continue?");
			ch = sc.next().charAt(0);
			System.out.println();

		}while(ch=='Y'||ch=='y');
	}
}
