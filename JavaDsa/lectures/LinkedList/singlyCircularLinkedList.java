import java.util.*;

class Node{

	int data;
	Node next = null;

	Node(int data){
		
		this.data = data;
	}
}

class SCLL{

	Node head = null;

	void addFirst(int data){
		
		Node newNode = new Node(data);

		if(head==null){
		
			head = newNode;
			head.next = head;
		}else{
		
			Node temp = head;

			while(temp.next != head){
			
				temp = temp.next;
			}

			temp.next = newNode;
			newNode.next = head;
			head = newNode;
		}

	}

	void addLast(int data){
	
		Node newNode = new Node(data);

		if(head==null){
		
			head = newNode;
			head.next = head;
		}	
		else{
		
			Node temp = head;

			while(temp.next != head){
			
				temp = temp.next;
			}

			temp.next = newNode;
			newNode.next = head;
		}
	}

	void addAtPos(int pos, int data){
		
		if(pos <= 0 || pos >= countNode()+2){
		
			System.out.println("Entered wrong input data.");
			return;
		}

		if(pos == 1){
			
			addFirst(data);
		}
		else if(pos == countNode()+1){
		
			addLast(data);
		}
		else{
			Node newNode = new Node(data);

			Node temp = head;

			while(pos-2 != 0){
				
				pos--;
				temp = temp.next;
			}
			newNode.next = temp.next;
			temp.next = newNode;
		}
	
	}

	void delFirst(){
			
		if(head == null){
		
			System.out.println("Nothing to delete");
			return;
		}

		else if(head.next == head){
		
			head = null;
		}

		else{
		
			Node temp = head;

			while(temp.next != head){
			
				temp = temp.next;
			}

			//head = head.next;
			//temp = head;

			temp.next = head.next;
			head = head.next;
		}
	}

	void delLast(){
	
		if(head == null){
		
			System.out.println("Nothing to delete");
			return;
		}

		else if(head.next == head){
		
			head = null;
		}

		else{
		
			Node temp = head;

			while(temp.next.next != head){
				
				temp = temp.next;
			}

			temp.next = head;
		}
	
	}

	void delAtPos(int pos){
	
		if(pos <= 0 || pos >= countNode()+1){
		
			System.out.println("Entered wrong input data.");
			return;
		}

		if(pos == 1){
			
			delFirst();
		}
		else if(pos == countNode()){
		
			delLast();
		}
		else{
		
			Node temp = head;

			while(pos-2 != 0){
			
				pos--;
				temp = temp.next;
			}

			temp.next = temp.next.next;
		}
	
	}

	int countNode(){
		
		int count = 0;
		
		if(head==null){

			return count;
		}

		Node temp = head;

		while(temp.next != head){
		
			count++;
			temp = temp.next;
		}

		count++;

		return count;
	}

	void printCll(){
	
		if(head == null){
		
			System.out.println("Empty Linked list");
			return;
		}

		else{
		
			Node temp = head;

			while(temp.next != head ){
				
				System.out.print(temp.data + " -> ");
				temp = temp.next;
			}
			System.out.println(temp.data);
		}
	}

}

class Client{

	public static void main(String... args){
		
		SCLL obj = new SCLL();
		Scanner sc = new Scanner(System.in);
		char ch;
		do{
			System.out.println("\n************ Select from Options **************\n\n");

			System.out.println("1. Add First");
			System.out.println("2. Add Last");
			System.out.println("3. Add at Position");
			System.out.println("4. Delete First");
			System.out.println("5. Delete last");
			System.out.println("6. Delete at Position");
			System.out.println("7. Count Nodes");
			System.out.println("8. Print LinkedList\n");
	
			System.out.print("Enter your choice : ");

			int choice = sc.nextInt();

			switch(choice){
			
				case 1 : {
						System.out.print("\nEnter the data : ");
						int data = sc.nextInt();
						obj.addFirst(data);	
				
					}
					break;

				case 2: {
				
						System.out.print("\nEnter the data : ");
						int data = sc.nextInt();
						obj.addLast(data);	
				
					}
					break;
				case 3 : {
				
						System.out.print("\nEnter the position : ");
						int pos = sc.nextInt();

						System.out.print("\nEnter the data : ");
						int data = sc.nextInt();

						obj.addAtPos(pos,data);	
					}	
					break;

				case 4 : {
				
						obj.delFirst();
						break;
					}

				case 5: {
				
						obj.delLast();
						break;
					}

				case 6: {
						System.out.print("\nEnter the positotn to delete : ");
						int pos = sc.nextInt();
						obj.delAtPos(pos);
					}
					break;

				case 7:{
				
						int count = obj.countNode();
						System.out.println("\nTotal Nodes count : "+count);
					}
				       break;

				case 8:{

						obj.printCll();
					}
				       break;

				default: 
					System.out.println("\nWrong Input..");
			}


			System.out.print("\nDo you want to continue..?  ");

			ch = sc.next().charAt(0);

            	 }while(ch=='y'||ch=='Y');

	}
}
