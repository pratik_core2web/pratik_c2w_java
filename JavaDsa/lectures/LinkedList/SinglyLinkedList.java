

class LinkedList{

	Node head = null;

	class Node{
	
		int data ;

		Node next = null;

		Node(int data){
		
			this.data = data;
		}
	}

	void addFirst(int data){
		
		Node newNode = new Node(data);

		if(head == null){
			
			head = newNode;
		}else{
		
			newNode.next = head;
			head = newNode;
		}
		
	}

	void addLast(int data){

		Node newNode = new Node(data);
	
		if(head == null){
		
			head = newNode;
		}else{
		
			Node temp = head ;

			while(temp.next != null){
			
				temp = temp.next;
			}

			temp.next = newNode;
		}
	}

	void addAtPos(int pos , int data){
	
		if(pos<=0 || pos>=countNode()+2){
		
			System.out.println("Wrong Input");
		}

		if(pos==1){
		
			addFirst(data);

		}else if(pos==countNode()+1){
		
			addLast(data);

		}else{
		
			Node newNode = new Node(data);
			Node temp = head;

			while(pos - 2 != 0){
			
				temp = temp.next;
			}

			newNode.next = temp.next;
			temp.next = newNode;
		}
	}

	void delFirst(){
	
		if(head==null){
		
			System.out.println("Empty LinkedList");

		}
		else if(countNode()==1){
			
			head = null;
		}
		else{
		
			head = head.next;
		}
	}

	void delLast(){
	
		if(head==null){
		
			System.out.println("Empty LinkedList");
		}
		else if(countNode()==1){
			
			head = null;
		}
		else{
			Node temp = head;

			while(temp.next.next != null){
			
				temp = temp.next;
			}
			temp.next = null;
			
		}
	
	}

	void delAtPos(int pos){
	
		if(pos<=0 || pos>=countNode()+1){
		
			System.out.println("Wrong Input");
			return;

		}else if(pos==1){
			
			delFirst();

		}else if(pos==countNode() ){
		
			delLast();
		}else{
		
			Node temp = head;

			while(pos-2 !=0){
			
				pos--;
				temp = temp.next;
			}

			temp.next = temp.next.next;
		}
	}

	int countNode(){
	
		Node temp = head;
		int count = 0;

		while(temp!=null){
			
			count++;
			temp = temp.next;
		}

		return count;
	}

	void printsll(){
	
		if(head==null){
		
			System.out.println("Empty LinkedList");
		}else{
		
			Node temp = head;

			while(temp!=null){
			
				System.out.print(temp.data+"\t");

				temp = temp.next;
			}
			System.out.println();
		}
	}
}

class Client{

	public static void main(String... args){
		
		LinkedList ll = new LinkedList();

		ll.addFirst(10);
		ll.addFirst(20);
		ll.addFirst(30);
		ll.printsll();  //30 20 10

		ll.addLast(40);
		ll.addLast(50);
		ll.printsll();  //30 20 10 40 50 

		ll.addAtPos(2,15);	
		ll.printsll();	//30 15 20 10 40 50 

		ll.delFirst();
		ll.printsll();	//15 20 10 40 50
		
		ll.delLast();
		ll.printsll();	//15 20 10 40 

		ll.delAtPos(2);
		ll.printsll();	//15 10 40
	}
}
