

class Node{

	int data;
	Node next = null;

	Node(int data){
		this.data = data;
	}
}

class LinkedList{
	
	Node head = null;
	
	//count node
	int countNode(){
	
		Node temp = head;
		int count = 0;

		while(temp!=null){
		
			count++;
			temp=temp.next;
		}

		return count;
	}

	//add Last
	void addLast(int data){
	
		Node newNode = new Node(data);

		if(head == null){
			
			head = newNode;
		}else{
			
			Node temp = head;

			while(temp.next!=null){
			
				temp=temp.next;
			}
			temp.next = newNode;

		}
	}

	//middle of LinkedList
	
	int middleElement(){

		Node temp = head;
		
		int length = countNode();
		int count = 0;

		while(count < length/2){
			temp = temp.next;
			count++;
		}

		return temp.data;
	}
	
	//middle using slow and fast pointer
	int middleBySlowAndfastPointer(){
		
		if(head == null){
			
			return -1;
		}

		Node slow = head;
		Node fast = head.next;    //head
		
		while(fast!=null){   //fast.next!=null
		
			fast = fast.next;   //fast.next.next

			if(fast != null){   //
				  
				fast = fast.next;  //
			}                          //

			slow = slow.next;
		}

		return slow.data;
		
	}

	//print SLL
	void printsll(){
	
		if(head == null){
		
			System.out.println("Empty LinkedList");
		}
		else{
		
			Node temp = head;

			while(temp!=null){   //while(temp.next!=null)
				
				System.out.print(temp.data+"\t");
				temp=temp.next;
			}
			System.out.println();
	
		}	///sop(temp.data);
	}

	void reverseLL(){
		
		if(head == null){
		
			System.out.println("Empty LL");
			return;
		}

		Node prev = null;
		Node curr = head;
		Node forward = null;

		while(curr!=null){
		
			forward = curr.next;
			curr.next = prev;
			prev = curr;
			curr = forward;
		}
		head = prev;
	}

	void reverseRecc(Node curr, Node prev){

		if(curr==null){
			head = prev;
			return;
		}
		else{
			
			Node forward = curr.next;
			curr.next = prev;
			prev = curr;
			curr = forward;
		}

		reverseRecc(curr,prev);
	
	}
}

class Client{

	public static void main(String[] args){
	
		LinkedList ll = new LinkedList();

		ll.addLast(10);
		ll.addLast(20);
		ll.addLast(30);
		ll.addLast(40);
		ll.addLast(50);


		ll.printsll();

		ll.reverseLL();
		ll.printsll();


		LinkedList ll1 = new LinkedList();

		ll1.addLast(60);
		ll1.addLast(70);
		ll1.addLast(80);
		ll1.addLast(90);
		ll1.addLast(100);


		ll1.printsll();
		
		Node curr = ll1.head;
		Node prev = null;

		ll1.reverseRecc(curr,prev);
		ll1.printsll();

		LinkedList ll2 = new LinkedList();

		ll2.addLast(60);
		ll2.addLast(70);
		ll2.addLast(80);
		ll2.addLast(90);
		ll2.addLast(100);
		ll2.addLast(10);
		ll2.addLast(20);
		ll2.addLast(30);
		ll2.addLast(40);

		ll2.printsll();

		//int middle = ll2.middleElement();
		
		int middle = ll2.middleBySlowAndfastPointer();

		if(middle==-1){
			System.out.println("Empty Linked list");
		}else{
			System.out.println("Middle element : "+middle);
		}
	}
}
