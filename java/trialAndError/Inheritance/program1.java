

class Coach{
	
	Coach(){
		System.out.println("Coach Construstor");
	}
	void coaching(){
		System.out.println("overall planing");
	}
}

class BattingCoach extends Coach {

	void coaching(){
		System.out.println("Batting strategies");
	}
}

class BowlingCoach extends Coach {

	void coaching(){
	        super.coaching();
		System.out.println("Bowling strategies"); 
	}

	public static void main(String [] args){
		
		BowlingCoach obj1 = new BowlingCoach();
		BattingCoach obj2 = new BattingCoach();

		obj1.coaching();
		obj2.coaching();
	}
}

