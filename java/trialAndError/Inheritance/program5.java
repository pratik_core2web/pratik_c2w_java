


class Demo{

	Demo(){
		System.out.println(this);
	}

	Demo( int x){
		System.out.println(this);
	}
}

class User{
	public static void main(String[] args){
		Demo obj1 = new Demo();
		Demo obj2 = new Demo(20);

		System.out.println(obj1);
		System.out.println(obj2);
	}
}
