


class ShowingHighlightsException extends Exception{
	
	ShowingHighlightsException(String str){
		super(str);
	}
}

class Hotstar{
	
	boolean rainStatus = false;

	boolean rainCheck(){
		return rainStatus;
	}

	void match(String str) throws ShowingHighlightsException {

		boolean status = rainCheck();
		
		try{
			if(status == true){

				throw new ShowingHighlightsException("IndVsNz match is interrupted by rain so we are showing old matches highlights, we'll back soon..");
			}
			
			else{
				System.out.println("Enjoy " +str);
			}

		}catch(ShowingHighlightsException e){

			e.printStackTrace();

			System.out.println("\n"+e);

			System.out.println("\n"+e.getMessage());

		}
	}
}

class User{

	public static void main(String[] args) throws ShowingHighlightsException{

		Hotstar obj = new Hotstar();
		
		obj.match("IndVsNz");
	}
}















