


class Outer{

	int x = 10;
	
	class Inner{
		
		int x = 20;

		Inner(){
			
			System.out.println(this.x);  //20
		
			System.out.println(Outer.this.x);  //10

		}
	}

	public static void main(String[] args){

		Outer.Inner obj3 = new Outer().new Inner();
		
	}
}


