

class Outer{

	void fun(){
		System.out.println("In fun");
	
		class One{
			One(){	
				System.out.println("In one");
			}

			void gun(){
				System.out.println("In gun");

				class two{
					two(){
 						System.out.println("In two");
					}
				}
				two obj3 = new two();
			}
		}

		One obj2 = new One();
		obj2.gun();
	
	}

	public static void main(String[] args){
		Outer obj1 = new Outer();
		obj1.fun();
	}
}


