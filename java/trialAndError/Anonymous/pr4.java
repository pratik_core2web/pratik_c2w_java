


class One{

	void run(){
		System.out.println("In run-One");
	}

	void fun(){
		System.out.println("In fun-One");
	}

}

class Two{

	public static void main(String[] args){

		One obj = new One(){
			
			void fun(){
				System.out.println("In Fun-Two$1");
			}
		
			void run(){
				System.out.println("In run-Two$1");
			}
		};
	
		obj.fun();    //In fun-Two$1  child chi method
		obj.run();    //In run-one    child kd ahe so child chi ch honar

	}
}


