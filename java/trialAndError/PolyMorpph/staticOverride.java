



class Parent{

	static void fun(){
	
		System.out.println("In fun parent");
	}
	void gun(){
	
		System.out.println("In gun parent");
	}
}

class Child extends Parent{

	static void fun(){ 
	
		System.out.println("In fun child");
	} 
	void gun(){
	
		System.out.println("In gun child");
	}
}
class User{

	public static void main(String[] args){
	
		Parent p = new Child();

		p.fun();
		p.gun();
	}
}
