


import java.util.Scanner;

class ScannerDemo3{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter name: ");
		String name = sc.next();

		System.out.print("Enter clg name: ");
		String clgName = sc.next();

		System.out.print("Enter stud id: ");
                int studId = sc.nextInt();

		System.out.print("Enter CGPA: ");
		float cgpa = sc.nextFloat();

		System.out.println(name);
		System.out.println(clgName);
		System.out.println(studId);
		System.out.println(cgpa);
	}
}
