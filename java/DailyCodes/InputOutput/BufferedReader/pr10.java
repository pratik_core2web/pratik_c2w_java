


import java.io.*;
class Demo10{
	public static void main(String[] args)throws IOException {
		InputStreamReader isr = new InputStreamReader(System.in);
                BufferedReader br = new BufferedReader(isr);

		String name = br.readLine();
	        System.out.println("Name: "+name);
		
		br.close();

		String cmpName = br.readLine();
	        System.out.println("Comp Name: "+cmpName);
	}
}

