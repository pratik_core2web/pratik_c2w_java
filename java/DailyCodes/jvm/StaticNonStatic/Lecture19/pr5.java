


class InputDemo4{

	void fun(){     //non-static method
		System.out.println("In fun function");
	}

	static void run(){     //static method
		System.out.println("In run function");
	}


	public static void main(String[] args){
		System.out.println("In main method");
		InputDemo4.run();  //also a valid method to call static method
	}
}

	


