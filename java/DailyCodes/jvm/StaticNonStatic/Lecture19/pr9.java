


class InputDemo9{

	void methodFun(){     //non-static method
		System.out.println("In fun function");
	}

	void methodGun(){     //non-static method
		System.out.println("In fun function");
	}

	void methodRun(){     //non-static method
		System.out.println("In run function");
	}


	public static void main(String[] args){
		System.out.println("In main method");
		
		InputDemo9 obj = new InputDemo9();

		obj.methodFun();
		obj.methodGun();
		obj.methodRun();
	}
}

	


