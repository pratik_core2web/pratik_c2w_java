


class InputDemo6{

	void fun(){     //non-static method
		System.out.println("In fun function");
	}

	static void run(){     //static method
		System.out.println("In run function");
	}


	public static void main(String[] args){
		System.out.println("In main method");
		run();  //also a valid method to call static method

		
		InputDemo6 obj = new InputDemo6();
		obj.fun();
	}
}

	


