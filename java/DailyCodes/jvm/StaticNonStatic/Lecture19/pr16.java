



class StaticDemo6{

	int x = 30;
	static int y = 40;

	void fun(){
		System.out.println("In fun metthod");
	}

	static void run(){
		System.out.println("In run metthod");
	}


	public static void main(String[] args){

		StaticDemo6 obj = new StaticDemo6();

		System.out.println(obj.x);
		System.out.println(obj.y);
		obj.fun();
		obj.run();
	}
}

