



class StaticDemo5{

	int x = 30;
	static int y = 40;

	void fun(){
		System.out.println("In fun metthod");
	}

	static void run(){
		System.out.println("In run metthod");
	}


	public static void main(String[] args){

		StaticDemo5 obj = new StaticDemo5();

		System.out.println(x);
		System.out.println(obj.y);
		fun();
		obj.run();
	}
}

