



class StaticDemo4{

	int x = 30;
	static int y = 40;

	void fun(){
		System.out.println("In fun metthod");
	}

	static void run(){
		System.out.println("In run metthod");
	}


	public static void main(String[] args){

		StaticDemo4 obj = new StaticDemo4();

		obj.fun();
		obj.run();
	}
}

