


class Demo{

	private int x = 10;
	int y = 20;

	private void fun(){
	
		System.out.println("In Private fun-demo");
	}

	void run(){
	
		System.out.println("In run-demo");
		fun();
		this.run(10);
	}

	void run(int x){
	
		System.out.println(x);
	}
}

class Pepo{

	public static void main(String... args){
	
		Demo o = new Demo();

		o.run();
	}
}
