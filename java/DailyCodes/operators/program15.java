




//signed negative shift works life positive numbers but after finding it positive shift then use the negation property..
//
//for e.g -->   int x = 10;
//              sop(x>>2);
//              then x = 2;
//
//              for finding negative right shift of ;
//              int x = -10;
//
//              simply understand that it works on negation trick.. i.e { 10 + 1 = 11,then give (-) sign so, -11 is answer}



class negativeRightShift{

	public static void main(String [] args){

	     int x = -9;  //0b00001001

	     System.out.println(x>>2);  //-3

    }
}



