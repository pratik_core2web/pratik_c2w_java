



//BitwiseOperator & , | , ^ (XOR) , ~ , << (left shift), >> (signed right shift), >>> (Unsigned right shift)


//& = 1 + 1 = 1 only  i.e && table
//| = 0 + 0 = 0 0nly  i.e || table
//^ = 0/1 + 0/1 = 0  i.e same same = zero



class Bitwise1{

	public static void main(String[] args){

		int x = 10;                //0b00001010
		int y = 12;                //0b00001100


		System.out.println(x & y); //0b00001000  //8 
	        System.out.println(x | y); //0b00001110	 //14
		System.out.println(x ^ y); //0b00000110  //6
	}
}

