

import java.util.*;

class Demo{
	
	String empName;
	int empId;

	Demo(String empName,int empId){
		
		this.empName = empName;
		this.empId = empId;
	}

	public int hashCode(){
		return empId*2+5 ;
	}

	public String toString(){
		return "{"+ empName+ ":"+ empId+ "}";
	}
}

class HashCodeDemo{
	
	public static void main(String[] args){

		Hashtable ht = new Hashtable();
		
		Demo obj1 = new Demo("Onkar",15);
		Demo obj2 = new Demo("Om",45);
		Demo obj3 = new Demo("Durgya",20);
		Demo obj4 = new Demo("Shlok",50);
		
		ht.put(obj1,"Flutter");
		ht.put(obj2,"Java");
		ht.put(obj3,"Gate");
		ht.put(obj4,"Spring");

		System.out.println(ht);
	}
}
