import java.util.*;

class WWE {

	String wrestler;
	double weight;

	WWE(String wrestler,double weight){
		
		this.wrestler = wrestler;
		this.weight = weight;
	}

	public String toString(){
		
		return "{"+wrestler+ ": "+weight+ "}";
	}
}

class SortByWeight implements Comparator<WWE>{

	public int compare(WWE obj1, WWE obj2){
		
		return (int)(obj1.weight-obj2.weight);
	}
}

class SortByName implements Comparator<WWE> {

	public int compare(WWE obj1, WWE obj2){
		
		return obj1.wrestler.compareTo(obj2.wrestler);
	}
}

class ComparatorDemo{

	public static void main(String[] args){
	
		TreeMap<WWE,String> tm = new TreeMap(new SortByName());

		WWE obj1 = new WWE("TheRock",95);
		WWE obj2 = new WWE("RomanReigns",105);
		WWE obj3 = new WWE("SethRollins",85);
		WWE obj4 = new WWE("Khali",150);

		tm.put(obj1,"USA");
		tm.put(obj2,"AUS");
		tm.put(obj3,"CANADA");
		tm.put(obj4,"INDIA");

		System.out.println(tm);
	}
}


