



class Meow{
	
	public Meow(){
	
		System.out.println("Meow from Constructor");
	}

	static Meow run(){
	
		return new Meow();
	}
}

class Cat{
	public static void main(String[] args){
	
		Meow.run();
	}
}
