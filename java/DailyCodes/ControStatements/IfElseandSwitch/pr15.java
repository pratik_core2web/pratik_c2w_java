



class SwitchDemo7{

	public static void main(String[] args){

		int num = 1;
		System.out.println("B4 Switch..");

		switch(num){

			case 1:
				System.out.println("Statement 1");
			//	break;

			case 2:
				System.out.println("Statement 2");
				break;

			case 3:
				System.out.println("Statement 3");
				break;

			default:	
				System.out.println("In default statement");
		}

		System.out.println("After Switch...");
	}
}

