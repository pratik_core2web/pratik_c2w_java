






class SwitchDemo12{
	public static void main(String[] args){

		String friends = "Kanha";
                System.out.println("B4 switch statement");

		switch(friends){

			case "Ashish" :
				System.out.println("Barclays");
				break;

			case "Kanha" :
				System.out.println("BMC software");
				break;

			case "Rahul": 	
				System.out.println("Infosys");
				break;

			case "Badhe": 	
				System.out.println("IBM");
				break;

			default:	
				System.out.println("In default statement");
		}

		System.out.println("After switch");
	}
}

				


	
