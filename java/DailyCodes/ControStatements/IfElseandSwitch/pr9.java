



class SwitchDemo1{

	public static void main(String[] args){

		int num = 3;
		System.out.println("B4 Switch..");

		switch(num){

			case 1:
				System.out.println("Statement 1");

			case 2:
				System.out.println("Statement 2");

			case 3:
				System.out.println("Statement 3");
		}

		System.out.println("After Switch...");
	}
}

