






class SwitchDemo10{
	public static void main(String[] args){

		char data = 'B';
                System.out.println("B4 switch statement");

		switch(data){

			case 'A' :
				System.out.println("A");
				break;

			case 'B' :
				System.out.println("B");
				break;

			case 'C': 	
				System.out.println("C");
				break;

			default:	
				System.out.println("In default statement");
		}

		System.out.println("After switch");
	}
}

				


	
