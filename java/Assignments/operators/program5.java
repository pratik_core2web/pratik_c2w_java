




class Bitwise{

	public static void main(String [] args){

		int x = 9;  //0b00000000000000000000000000001001 
		int y = 6;  //0b00000110

		System.out.println(x&y); //0
		System.out.println(x|y); //15
		System.out.println(x^y); //15
		System.out.println(x<<6); //0b00000000000000000000001001000000  //576
		System.out.println(x>>y); //0b00000000
		System.out.println(x>>>y); //0
	}
}

