



class Basics{

	public static void main(String [] args){

		int x = 40;
		int y = 5;

		System.out.println(x+y); //45
		System.out.println(x-y); //35
		System.out.println(x/y); //8
		System.out.println(x*y); //200
		System.out.println(x%y); //0


		System.out.println(x+=5); //x=45
                System.out.println(x-=5);  //x=40
		System.out.println(x*=5);  //x=200
		System.out.println(x/=5);  //x=40
		System.out.println(x%=5);  //x=0

		System.out.println(x==y);  //false
		System.out.println(x!=y);  //true
		System.out.println(x<y);  //true
		System.out.println(x>y);  //false
		System.out.println(x<=y); //true
		System.out.println(x>=y); //false

		System.out.println(++x);  //1
		System.out.println(x++);  //1
	        System.out.println(--x);  //1
		System.out.println(x--);  //1 but x==0


		System.out.println(x<<2); //0
		System.out.println(x>>3);//0
		System.out.println(x>>>2);//0
		System.out.println(x^y); //5
		System.out.println(x&y); //0
                System.out.println(x|y); //5		

		boolean c = true;
		boolean b = false;

		System.out.println(!c); //f
		System.out.println(!b); //t
		}
}

