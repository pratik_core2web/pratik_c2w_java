




class Boolean{

	public static void main(String[] args){

		boolean x = true;
		boolean y = false;

		System.out.println(x&&y); //f
		System.out.println(x||y);  //t
		System.out.println(!x);    //f
		System.out.println(!y);    //t
	}
}

