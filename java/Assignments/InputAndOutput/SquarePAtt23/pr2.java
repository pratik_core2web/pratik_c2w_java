/*2. WAP in notebook & Dry run first then type
Take the number of rows from the user :
row=3
c d E
f G H
I J K
row=4
d e f G
h i J K
l M N O
P Q R S*/

import java.util.*;
class Square2{
	public static void main(String args[]){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the rows : ");
		int row = sc.nextInt();

		int num = row+96;

		for(int i = 1; i<=row; i++){
			for(int j = 1; j<=row; j++){
				if(j<=row-i)
					System.out.print((char)num+"\t");
				else
					System.out.print((char)(num-32)+"\t");

				
				num++;
			}
			System.out.println();
		}
	}
}
