/*1. WAP in notebook & Dry run first then type
Take a number of rows from the user :
row=3
C d e
F g h
I j k
row=4
D e f g
H i j k
L m n o
P q r s*/

import java.util.*;
class Square1{
	public static void main(String args[]){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the rows : ");
		int row = sc.nextInt();

		int num = row+64;

		for(int i = 1; i<=row; i++){
			for(int j = 1; j<=row; j++){
				if(j==1)
					System.out.print((char)num+"   ");
				else
					System.out.print((char)(num+32)+"   ");
				num++;
			}
			System.out.println();
		}
	}
}
