/*6.WAP in notebook & Dry run first then type
Take number of rows from user :

row=3

    1
  1 2 3
1 2 3 4 5
row=4

      1
    1 2 3
  1 2 3 4 5
1 2 3 4 5 6 7*/

import java.io.*;
class Sqr6{
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<=row; i++){
			for(int spc = 1; spc<=row-i; spc++){
				System.out.print("\t");
			}
                        
			int num = 1;
			
			for(int j = 1; j<=i*2-1; j++){
				System.out.print(num++ +"\t");
				
			}
			System.out.println();
		}
	}
}


