/*1. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
    1
  3 5
7 9 11
row=4

         1
       3 5
    7  9 11
13 17 19 23*/

import java.io.*;
class Sqr1{
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());

		int num = 1;

		for(int i = 1; i<=row; i++){
			for(int spc = 1; spc<=row-i; spc++){
				System.out.print("\t");
			}

			for(int j = 1; j<=i; j++){
				System.out.print(num+"\t");
				num+=2;
			}
			System.out.println();
		}
	}
}


