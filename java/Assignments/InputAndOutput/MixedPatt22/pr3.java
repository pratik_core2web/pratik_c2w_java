/*3. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3

      1
    4 7
10 13 16
row=4

            1
        5   9
    13  17  21
25  29  33  37*/

import java.io.*;
class Sqr3{
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());

		int num = 1;

		for(int i = 1; i<=row; i++){
			for(int spc = 1; spc<=row-i; spc++){
				System.out.print("\t");
			}

			for(int j = 1; j<=i; j++){
				System.out.print(num +"\t");
				num+=row;
			}
			System.out.println();
		}
	}
}


