/*2. WAP in notebook & Dry run first then type
Take number of rows from user :
row = 3
1 2 3
3 4
4
row =4
1 2 3 4
4 5 6
6 7
7*/

import java.io.*;
class Sqr2{
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());

		int num = 1;

		for(int i = 1; i<=row; i++){
		
			for(int j = 1; j<=row+1-i; j++){
				System.out.print(num++ +"\t");
			}
			num--;
			System.out.println();
		}
	}
}


