/*9.WAP in notebook & Dry run first then type
Take number of rows from user :

row=3

    3
  4 3 2
5 4 3 2 1

row=4

      4
    5 4 3
  6 5 4 3 2
7 6 5 4 3 2 1*/

import java.io.*;
class Sqr9 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());

		int num1 = row;
		int num2 = 0;

		for(int i = 1; i<=row; i++){
			for(int spc = 1; spc<=row-i; spc++){
				System.out.print("\t");
			}
			
			for(int j = 1; j<=i*2-1; j++){
				System.out.print(num1-- +"\t");
			}
                        //num2 = (i*2-1) + num1;
                        //num1 = num2 + 1;
			num1 = (num1+1) + (i*2-1);
			System.out.println();
		}
	}
}


