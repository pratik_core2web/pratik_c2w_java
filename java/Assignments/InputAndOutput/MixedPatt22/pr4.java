/*4. WAP in notebook & Dry run first then type
Take number of rows from user : (check the row is even or odd if odd print Capital
letters else print small letters)
row=3
A B C
  B C
   C

row=4
a b c d
  b c d
    c d
      d*/

import java.io.*;
class Sqr4{
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<=row; i++){
			for(int spc = 1; spc<i; spc++){
				System.out.print("\t");
			}
                        int num = i+64;
			for(int j = 1; j<=row+1-i; j++){

				if(row%2==1)
		        		System.out.print((char)(num) +"\t");

				else
		        		System.out.print((char)(num+32) +"\t");

				num++;
			}
			System.out.println();
		}
	}
}


