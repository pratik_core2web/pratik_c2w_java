/*10. Write a program to print the given pattern
rows=3
1
b c
4 5 6

rows=4
1
b c
4 5 6
g h i j*/

import java.io.*;
class Pattern10{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the Rows : ");
		int row = Integer.parseInt(br.readLine());
                
         	char ch = 'a';

		for(int i = 1; i<=row; i++){
			for(int j = 1; j<=i; j++){

				if(i%2==1){
					System.out.print((int)(ch-96) + "   ");
					
				
				}
				else{
					System.out.print(ch +"   ");
				}
				ch++;
			
			}
			System.out.println();
		}
	}
}



