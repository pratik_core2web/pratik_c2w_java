/*4.Write a program to print the given pattern
rows=3
c
C B
c b a
rows=4
d
D C
d c b
D C B A*/

import java.io.*;
class Pattern4{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the Rows : ");
		int row = Integer.parseInt(br.readLine());
                

		for(int i = 1; i<=row; i++){
			char ch = (char)(row+64);
			for(int j = 1; j<=i; j++){

				if(i%2==1){
					System.out.print((char)(ch+32) + "   ");
					
				
				}
				else{
					System.out.print(ch +"   ");
				}
				ch--;
			
			}
			System.out.println();
		}
	}
}



