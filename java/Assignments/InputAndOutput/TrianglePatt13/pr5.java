/*5. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
A B C
a b
A

Rows = 4
A B C D
a b c
A B
a*/


import java.io.*;
class Triangle5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<=row; i++){
			int num = 65;

			for(int j = 1; j<=row+1-i; j++){

				if(i%2==1)
				       System.out.print((char)(num)+ "\t");

				else
				       System.out.print((char)(num+32)+ "\t");
				       
				num++;
			}
			System.out.println();
		}
	}
}


                                 
