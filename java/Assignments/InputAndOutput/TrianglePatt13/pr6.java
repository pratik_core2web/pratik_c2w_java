/*6. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
1 a 2
1 a
1
Rows = 4
1 a 2 b
1 a 2
1 a
1*/


import java.io.*;
class Triangle6{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<=row; i++){
			int num = 1;

			for(int j = 1; j<=row+1-i; j++){

				if(j%2==1)
				       System.out.print(num+ "\t");

				else{
				       System.out.print((char)(num+96)+ "\t");
				       num++;
				}
				
			}
			System.out.println();
		}
	}
}


                                 
