/*7. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
4 c 2 a
3 b 1
2 a
1
Rows = 5
5 d 3 b 1
4 c 2 a
3 b 1
2 a
1*/


import java.io.*;
class Triangle7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<=row; i++){
			int num = row+1-i;

			for(int j = 1; j<=row+1-i; j++){

				if(j%2==1)
				       System.out.print(num+ "\t");

				else{
				       System.out.print((char)(num+96)+ "\t");
				}
				num--;
				
			}
			System.out.println();
		}
	}
}


                                 
