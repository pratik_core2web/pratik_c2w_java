/*4. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
F E D
C B
A
Rows = 4
J I H G
F E D
C B
A*/


import java.io.*;
class Triangle4{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());
                
		int num = (row * (row+1))/2;

		for(int i = 1; i<=row; i++){
			for(int j = 1; j<=row+1-i; j++){
				System.out.print((char)(num+64)+ "\t");
				num--;
			}
			System.out.println();
		}
	}
}


                                 
