

/*10. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
c b a
B A
a
Rows = 4
D C B A
c b a
B A
a*/


import java.io.*;
class Triangle10 {
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<=row; i++){
			int num = (row+1-i)+64;

			for(int j = 1; j<=row+1-i; j++){
				
				if(row%2==1){
					if(i%2==1)
						System.out.print((char)(num+32) + "\t");

					else
						System.out.print((char)num + "\t");
				}

				else{
					if(i%2==1)
						System.out.print((char)num + "\t");

					else
						System.out.print((char)(num+32) + "\t");
				}

				num--;

			}

			System.out.println();
		}
	}
}


                                 
