/*8. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
4 3 2 1
C B A
2 1
A
Rows = 5
5 4 3 2 1
D C B A
3 2 1
B A
1*/


import java.io.*;
class Triangle8 {
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<=row; i++){
			int num = row+1-i;

			for(int j = 1; j<=row+1-i; j++){

				if(i%2==1)
				       System.out.print(num+ "\t");

				else{
				       System.out.print((char)(num+64)+ "\t");
				}
				num--;
				
			}
			System.out.println();
		}
	}
}


                                 
