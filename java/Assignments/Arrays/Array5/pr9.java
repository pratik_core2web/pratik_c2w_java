/*Q9. WAP to take a number from the user and store each element in an array by increasing
the element by one.
Example:
Input:
Enter the Number:
1569872365
Output:
2, 6, 7, 10, 9, 8, 3, 4, 7, 6
Explanation : Each digit in a number is increased by one and stored in an array*/


import java.util.*;
class Arrray9 {
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number: ");
		int num = sc.nextInt();

		int temp = num;

		int count = 0;
		int lastD = 0;
		while(num>0){
			lastD = num %10;
			count++;
			num/=10;
		}
		System.out.println("Count of digits is : "+count);

		int arr[] = new int[count];

		for(int i = 0; i<count; i++){
			if(temp>0)
				arr[count-1-i]= temp%10 + 1;
			
			temp/=10;
		}

		System.out.print("Enter array increment elements: ");
		for(int i = 0; i<count; i++){
			System.out.print(arr[i]+"\t");
		}
		System.out.println();
	}
}
