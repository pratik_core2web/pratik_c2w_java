/*Q5. WAP to print the count of digits in elements of an array.
Example :
Input:
Enter the size of the array:
4
Enter the elements of the array:
1
225
32
356

Output:
1, 3, 2, 3*/

import java.util.*;
class Array5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size  : ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.print("Enter array elements  : ");
		for(int i = 0; i<size;i++){
			arr[i] = sc.nextInt();
		}

		int temp =0;
		int count = 0;

		for(int i = 0; i<size;i++){
			count = 0;
	          	while(arr[i]>0){
				arr[i]/=10;
				count++;
			}

			System.out.print(count+"\t");
		}
		System.out.println();
	}
}

