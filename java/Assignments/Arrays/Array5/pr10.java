/*Q10. WAP to print the factorial of each element in an array.
Example :
Input:
Enter the size of the array:
6
Enter the elements of the array:
1 2 3 5 8 2

Output:
1, 2, 6, 120, 40320, 2*/

import java.util.*;
class Array10 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of array: ");
		int size = sc.nextInt();

		long arr[] = new long[size];
		System.out.print("Enter elements ");

		for(int i=0; i<size; i++){
			arr[i]=sc.nextInt();
		}
 		
		long product = 1;
		long temp = 0;
		for(int i =0; i<size; i++){

			temp = arr[size-1-i];
			product = 1;

			while(temp>0){
				product = product*temp;
				temp--;
			}
			arr[size-1-i] = product;
		}

		for(int i =0; i<size; i++){
			System.out.print(arr[i]+"\t");
		}

		System.out.println();
	}
}


