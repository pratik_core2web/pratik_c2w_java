/*Q2. WAP to print the sum of odd and even numbers in an array.
Enter the size of the array:
6
Enter the elements of the array:
10
15
9
1
12
15
Output:
Odd Sum = 40
Even Sum = 22*/

import java.util.*;
class Arrray2 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Size of array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.print("Enter array elements: ");

		for(int i = 0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}
                
		int evenSum = 0;
		int oddSum = 0;

		for(int i = 0; i<arr.length; i++){
			if(arr[i]%2==0){
				evenSum+=arr[i];
			}
			else{
				oddSum+=arr[i];
			}

		}
		System.out.println("Odd sum : "+oddSum);
		System.out.println("Even sum : "+evenSum);
		
	}
}





