/*Q6. Write a program to check whether the array contains element multiple of
user given int value, if yes then print it’s index.
Example :
5 10 16 20 25 30 35 4 8 12 16 20
Input 1:
Enter key : 5
Output:
An element multiple of 5 found at index : 0
An element multiple of 5 found at index : 1
An element multiple of 5 found at index : 3
An element multiple of 5 found at index : 4
An element multiple of 5 found at index : 5
An element multiple of 5 found at index : 6
An element multiple of 5 found at index : 11*/

import java.util.*;
class A6 {
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		
		System.out.print("Enter size of First array: ");
		int size1 = sc.nextInt();
		
		int a[] = new int [size1];

		System.out.print("Enter array elements : ");
		for(int i =0; i<size1; i++){
			a[i]=sc.nextInt();
		}

		System.out.print("Enter key: ");
		int key = sc.nextInt();
		int flag = 0;

		for(int i =0; i<size1; i++){
			if(a[i] % key == 0){
				flag = 1;
				System.out.println("An element multiple of "+key+" is found at "+i);
			}
		}
		if(flag==0)
			System.out.println("Key not found..");

	}
}

		
		

