/*Q8. Write a program to reverse the char array and print the alternate
elements of the array before and after reverse.

Size = 10
A B C D E F G H I J

After reverse :

J I H G F E D C B A

Output :
Before Reverse:
A C E G I

After Reverse:

J H F D B*/

import java.util.*;
class A8 {
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		
		System.out.print("Enter size of First array: ");
		int size1 = sc.nextInt();
		
		int a[] = new int [size1];

		System.out.print("Enter array elements : ");
		for(int i =0; i<size1; i++){
			a[i]=sc.nextInt();
		}
		int temp =0;
		System.out.print("Before Reverse: ");
		for(int i = 0; i<=a.length/2; i++){
			System.out.print(a[temp]+"\t");
			temp+=2;
		}
		System.out.println();

		for(int i = 0; i<=a.length/2; i++){
			temp = a[i];
			a[i] = a[a.length-1-i];
			a[a.length-1-i] = temp;
		}

		temp =0;
		System.out.print("After Reverse: ");
		for(int i = 0; i<=a.length/2; i++){
			System.out.print(a[temp]+"\t");
			temp+=2;
		}
		System.out.println();
	}
}



		



		
		

