/*Q4. You have to take two different 1d arrays of the same size and print the
common elements from the arrays.
arr1
45 67 97 87 90 80

arr2
15 97 67 80 90 10

Output:
Common elements in the given arrays are: 67, 97, 90, 80*/

import java.util.*;
class A4 {
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		System.out.print("Enter size of array: ");
		int size = sc.nextInt();
		int arr1[] = new int [size];
		int arr2[] = new int [size];

		System.out.print("Enter elements of first array: ");
		for(int i=0; i<arr1.length; i++){
			arr1[i]=sc.nextInt();
		}

		System.out.print("Enter elements of second array: ");
		for(int i=0; i<arr2.length; i++){
			arr2[i]=sc.nextInt();
		}
		
		System.out.print("Enter common elements in both the arrays: ");
		for(int i=0; i<size;i++){
			
			for(int j=0;j<size;j++){
				
				if(arr1[i]==arr2[j])
					System.out.print(arr1[i]+"\t");

			}
		}
		
		System.out.println();
	}
}
		
		

