/*Q2. Write a program to find out the sum of all prime numbers in an array and
also print the count of prime numbers in an array.
Example:

5 7 9 12 17 19 21 22

Output:
Sum of all prime numbers is 48 and count of prime numbers in the given array is 4*/

import java.util.*;
class A2 {
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		System.out.print("Enter size of array: ");
		int size = sc.nextInt();
		int arr[] = new int [size];

		System.out.print("Enter elements of array: ");
		for(int i=0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}
 		
		int count = 0;
		int primeCount = 0;
		int temp = 0;
		int sum = 0;

		for(int i = 0; i<size; i++){
			temp = arr[i];
			count = 0;
		
			for(int j =1; j<=arr[i]/2; j++){
				if(temp%j==0)
					count++;
			}
			if(count==1){
				primeCount++;
				sum+=arr[i];
			}
		}
		System.out.println("Sum of prime numbers: "+sum);
		System.out.println("Total  prime numbers: "+primeCount);

	}
}
		

