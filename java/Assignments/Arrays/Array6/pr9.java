/*Q9. Write a program to count the palindrome elements in your array.
arr
121 1 58 333 616 9

Count of palindrome elements is : 5

(Single number is also a palindrome number)*/

import java.util.*;
class A9 {
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		
		System.out.print("Enter size of array: ");
		int size = sc.nextInt();
		
		int arr[] = new int [size];

		System.out.print("Enter array elements : ");
		for(int i =0; i<size; i++){
			arr[i]=sc.nextInt();
		}

		int lastD = 0;
		int rev = 0;
		int count = 0;
		int temp =0;

		for(int i =0; i<size; i++){
			temp =arr[i];
			rev = 0;	
			while(temp>0){
				lastD=temp%10;
				rev = lastD + rev*10;
				temp/=10;
			}

			if(arr[i]==rev){
			      //System.out.println(arr[i]+" is pallindrome.");
				count++;
			}
		}
		
		System.out.println("Count of pallindrome element is : "+count );
	}
}
