/*Q3. Write a program to check the count of the user given key in your array, if
the count is more than 2 times replace the element with its cube, print the
array.
Example:
arr1:
11 6 8 9 5 8 7 8 6

Input 1:
Enter key: 8
Output
Array will be like :
11 6 512 9 5 512 7 512 6*/

import java.util.*;
class A3 {
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		System.out.print("Enter size of array: ");
		int size = sc.nextInt();
		int arr[] = new int [size];

		System.out.print("Enter elements of array: ");
		for(int i=0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}

		System.out.print("Enter the key number: ");
		int key = sc.nextInt();
		int count=0;
		

		for(int i=0; i<arr.length; i++){
			if(arr[i]==key)	
				count++;
		}

		if(count==0)
			System.out.println("Number not found");

		if(count>2){
			for(int i=0; i<size; i++){
				if(arr[i]==key)
					arr[i]=arr[i]*arr[i]*arr[i];

				System.out.print(arr[i]+"\t");
			}
		}

/*		for(int i=0; i<arr.length; i++){
			System.out.print(arr[i]+"\t");
		}*/

		System.out.println();
	}
}

		
		

