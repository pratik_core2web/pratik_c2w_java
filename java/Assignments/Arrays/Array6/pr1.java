/*Q1. Write a program to check whether the given array is descending or not.
Input 1:
Enter the size of the array:
4
Enter the elements of the array:
15 9 5 1
Output :
Given array is in descending order.
Input 2:
Enter the size of the array:
4
Enter the elements of the array:
1 9 5 1
Output :
Given array is not in descending order.*/

import java.util.*;
class A1 {
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		System.out.print("Enter size of array: ");
		int size = sc.nextInt();
		int arr[] = new int [size];

		System.out.print("Enter elements of array: ");
		for(int i=0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}
 
		int flag = 0;
		for(int i  =0; i<arr.length-1; i++){
			if(arr[i]<=arr[i+1]){
				flag = 1;
				break;
			}
		}
		if(flag==1)
			System.out.println("Array is not in Descending ordeer");
		else
			System.out.println("Array is in Descending ordeer");
	}
}
		

