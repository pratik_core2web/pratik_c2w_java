/*Q5. Take two different arrays where size of array may differ, you have to
create an array by combining both the arrays (you have to merge the arrays)
Example :

arr1
5 10 15 20 25 30 35

arr2
4 8 12 16 20

Array after merger :

5 10 15 20 25 30 35 4 8 12 16 20*/

import java.util.*;
class A5 {
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		
		System.out.print("Enter size of First array: ");
		int size1 = sc.nextInt();
		
		System.out.print("Enter size of First array: ");
		int size2 = sc.nextInt();
		
		int a[] = new int [size1];
		int b[] = new int [size2];
		
		int c[] = new int [size1+size2];

		System.out.print("Enter elements of first array: ");
		for(int i=0; i<a.length; i++){
			a[i]=sc.nextInt();
		}
 
		System.out.print("Enter elements of second array: ");
		for(int i=0; i<b.length; i++){
			b[i]=sc.nextInt();
		}

		for(int i=0; i<a.length; i++){
			c[i]=a[i];
		}

		for(int i=0; i<b.length; i++){
			c[a.length+i] = b[i];
		}
		
		System.out.print("Merged array is:");
		for(int i=0; i<c.length; i++){
			System.out.print(c[i]+"\t");
		}

		System.out.println();
	}
}

		
		

