/*Q10. Write a program to find the third largest element in an array.
Example :
56 15 8 26 7 50 54

Output:
Third largest element is: 50*/

import java.util.*;
class A10 {
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		
		System.out.print("Enter size of array: ");
		int size = sc.nextInt();
		
		int arr[] = new int [size];

		System.out.print("Enter array elements : ");
		for(int i =0; i<size; i++){
			arr[i]=sc.nextInt();
		}

		int max = arr[0];
		for(int i =0; i<size; i++){
			if(arr[i]>max)
				max = arr[i];
		}
		System.out.println("Maximum element : "+max);

		int smax = arr[0];
		for(int i =0; i<size; i++){
			if(arr[i]>=smax && arr[i]<max)
				smax = arr[i];
		}
		System.out.println("Second Maximum element : "+smax);

		int tmax = arr[0];
		for(int i =0; i<size; i++){
			if(arr[i]>=tmax && arr[i]<max && arr[i]<smax)
				tmax = arr[i];
		}
		System.out.println("Third Maximum element : "+tmax);
	}
}

