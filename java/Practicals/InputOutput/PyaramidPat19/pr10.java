/*10.WAP in notebook & Dry run first then type
Take number of rows from user :

row=3

    C
  B C B
A B C B A

row=4

      D
    C D C
  B C D C B
A B C D C B A*/

import java.io.*;
class Pyramid10 {
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());                

		for(int i = 1; i<=row; i++){	

                        int num = 'A';   //row+1-i  +64;

			for(int spc = 1; spc<=row-i; spc++){
				System.out.print("\t");
				num++;
			}

			for(int j = 1; j<=i*2-1; j++){				

				if(j<i)
				       System.out.print((char)(num++) + "\t");
				else
				       System.out.print((char)(num--) + "\t");

			}

			System.out.println();
		}
	}
}
