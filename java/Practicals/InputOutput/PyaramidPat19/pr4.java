/*4.WAP in notebook & Dry run first then type
Take number of rows from user :
row=3

    A
  B B B
C C C C C
row=4

      A 
    B B B
  C C C C C
D D D D D D D*/

import java.io.*;
class Pyramid4 {
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());
                
		for(int i = 1; i<=row; i++){
                        
		        int num = 64+i;	

			for(int spc = 1; spc<=row-i; spc++){
				System.out.print("\t");
			}

			for(int j = 1; j<=i*2-1; j++){
				System.out.print((char)num +"\t");
			}

			System.out.println();
		}
	}
}
