/*9.WAP in notebook & Dry run first then type
Take number of rows from user :

row=3

    A
  a b a
A B C B A

row=4

      A
    a b a
  A B C B A
a b c d c b a*/

import java.io.*;
class Pyramid9 {
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());                
                

		for(int i = 1; i<=row; i++){	
	        	int num1 = 65;
			int num2 = 97;

			for(int spc = 1; spc<=row-i; spc++){
				System.out.print("\t");
			}

			for(int j = 1; j<=i*2-1; j++){				

				if(i%2==1){
					if(j<i)
		              	        	System.out.print((char)(num1++)+ "\t");
					else
		              	        	System.out.print((char)(num1--)+ "\t");

				}
				else{
					if(j<i)
		              	        	System.out.print((char)(num2++)+ "\t");
					else
		              	        	System.out.print((char)(num2--)+ "\t");
				}
			}

			System.out.println();
		}
	}
}
