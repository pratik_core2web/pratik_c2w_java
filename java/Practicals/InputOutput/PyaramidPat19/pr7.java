/*7.WAP in notebook & Dry run first then type
Take number of rows from user :

row=3

    1
  B B B
3 3 3 3 3
row=4

      1
    B B B
  3 3 3 3 3
D D D D D D D*/

import java.io.*;
class Pyramid7 {
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());                
		
		int num = 1;

		for(int i = 1; i<=row; i++){	

			for(int spc = 1; spc<=row-i; spc++){
				System.out.print("\t");
			}

			for(int j = 1; j<=i*2-1; j++){
				
				if(i%2==1){
					System.out.print(num+ "\t");
				}

				else{
					System.out.print((char)(num+64)+ "\t");
				}
			}

                        num++;
			System.out.println();
		}
	}
}
