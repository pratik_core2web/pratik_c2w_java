/*3.WAP in notebook & Dry run first then type
Take number of rows from user :
row=3

    3
  2 2 2
1 1 1 1 1
row=4

      4
    3 3 3
  2 2 2 2 2
1 1 1 1 1 1 1*/

import java.io.*;
class Pyramid3{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());
                
		for(int i = 1; i<=row; i++){
                        
		        int num = row+1-i;	

			for(int spc = 1; spc<=row-i; spc++){
				System.out.print("\t");
			}

			for(int j = 1; j<=i*2-1; j++){
				System.out.print(num +"\t");
			}

			System.out.println();
		}
	}
}
