/*8.WAP in notebook & Dry run first then type
Take number of rows from user :
row=4
1 2 3 4 3 2 1
  2 3 4 3 2
    3 4 3
      4

row=3

1 2 3 2 1
  2 3 2
    3 
*/

import java.io.*;
class Inverted8 {
	public static void main(String [] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("ENter the rows : ");
		int row =  Integer.parseInt(br.readLine());
                
		int num = 1;
		for(int i = 1; i<=row; i++){
			for(int spc = 1; spc<i; spc++){
				System.out.print("\t");
			}
                        
			num = i;
			int temp = num;
			for(int j=1; j<=(row-i)*2+1; j++){
				if(temp<row)
					System.out.print(num++ +"\t");
				else 
					System.out.print(num-- +"\t");
				temp++;
			}

			System.out.println();
		}
	}
}

