/*7.WAP in notebook & Dry run first then type
Take number of rows from user :
row = 4
A B C D C B A
  A B C B A
    A B A
      A

row = 3

A B C B A
  A B A
    A 
*/

import java.io.*;
class Inverted7 {
	public static void main(String [] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("ENter the rows : ");
		int row =  Integer.parseInt(br.readLine());
                
		
		for(int i = 1; i<=row; i++){
			for(int spc = 1; spc<i; spc++){
				System.out.print("\t");
			}
 
			for(int j=1; j<=(row-i)*2+1; j++){
				System.out.print((char)(64+j) +"\t");
			}

			System.out.println();
		}
	}
}

