/*10.WAP in notebook & Dry run first then type

Take number of rows from user :
rows:3

3 2 1 2 3
  2 1 2
    1

rows:4
4 3 2 1 2 3 4
  3 2 1 2 3
    2 1 2
      1 
*/

import java.io.*;
class Inverted10 {
	public static void main(String [] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("ENter the rows : ");
		int row =  Integer.parseInt(br.readLine());
                
		for(int i = 1; i<=row; i++){
			for(int spc = 1; spc<i; spc++){
				System.out.print("\t");
			}
                        
			int num = row + 1 - i ;
			
			for(int j=1; j<=(row-i)*2+1; j++){
				if(j<=((row-i)*2+1)/2)
					System.out.print(num-- +"\t");
				else 
					System.out.print(num++ +"\t");
				
			}

			System.out.println();
		}
	}
