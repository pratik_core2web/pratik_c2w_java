/*6.WAP in notebook & Dry run first then type
Take number of rows from user :
row = 4
D D D D D D D
  C C C C C
    B B B
      A

row = 3

C C C C C
  B B B
    A 
*/

import java.io.*;
class Inverted6 {
	public static void main(String [] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("ENter the rows : ");
		int row =  Integer.parseInt(br.readLine());
                
		
		for(int i = 1; i<=row; i++){
			for(int spc = 1; spc<i; spc++){
				System.out.print("\t");
			}
 
			int num = 64 + (row + 1 - i);
			for(int j=1; j<=(row-i)*2+1; j++){
				System.out.print((char)(num) +"\t");
			}

			System.out.println();
		}
	}
}

