/*  3.WAP in notebook & Dry run first then type
Take number of rows from user :
row=4
4 4 4 4 4 4 4
  3 3 3 3 3
    2 2 2
      1

row=3

3 3 3 3 3
  2 2 2
    1 */

import java.io.*;
class Inverted3 {
	public static void main(String [] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("ENter the rows : ");
		int row =  Integer.parseInt(br.readLine());
                
		int num = row;
		for(int i = 1; i<=row; i++){
			for(int spc = 1; spc<i; spc++){
				System.out.print("\t");
			}
 
			num = row + 1 - i;
			for(int j=1; j<=(row-i)*2+1; j++){
				System.out.print(num +"\t");
			}

			System.out.println();
		}
	}
}

