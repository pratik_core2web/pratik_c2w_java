/*Q5 Write a program to check whether the given number is composite or
not.
Row = 3
1
2 4
3 6 9
Rows = 4
1
2 4
3 6 9
4 8 12 16*/


import java.util.*;

class Mixed5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int row = sc.nextInt();

		int num = 1;
		for(int i = 1; i<=row; i++){
			num = i;
			for(int j = 1; j<=i; j++){
				System.out.print(num+"\t");
				num+=i;
			}
			System.out.println();
		}
	}
}

