/*
Q9 Write a program to reverse the given number.
Rows = 4
1 2 3 4
C B A
1 2
A
Rows = 5
1 2 3 4 5
D C B A
1 2 3
B A
1*/


import java.util.*;
class Mixed9{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                int row = sc.nextInt();
		
                for(int i = 1; i<=row; i++){
		        int ch = ((row+1-i)+64);

                        for(int j = 1; j<=row+1-i; j++){
		                
				if(i%2==1){
					System.out.print(j+"\t");
				}
				else{
					System.out.print((char)ch +"\t");
					ch--;
				}				
			}
			
			System.out.println();
		}
	}
}

	
