/*Q1 Write a program to print the following pattern
Number of rows = 4
1 2 3 4
5 6 7 8
9 10 11 12
13 14 15 16
Number of rows = 3
1 2 3
4 5 6
7 8 9*/

import java.util.*;
class Mixed1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
		int row = sc.nextInt();
                
                int num = 1;
		for(int i = 1; i<=row; i++){
			for(int j = 1; j<=row; j++){
				System.out.print(num++ +"\t");
			}
			System.out.println();
		}
	}
}


