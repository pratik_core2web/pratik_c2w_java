/*Q3 Write a program to print the following pattern
Number of rows = 3
C B A
1 2 3
C B A
Number of rows = 4
D C B A
1 2 3 4
D C B A
1 2 3 4*/


import java.util.*;
class Mixed3{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter rows: ");
                int row = sc.nextInt();
		
		for(int i = 1; i<=row; i++){
		        char ch = (char)(row+64);
			for(int j = 1; j<=row; j++){
				
				if(i%2!=0){
				        System.out.print(ch-- +"\t");
				}

				else{
					System.out.print(j+"\t");
				}
			}
			System.out.println();
		
		}
	}
}

