/*Q2 Write a program to print the following pattern
Number of rows = 3
C3 C2 C1
C4 C3 C2
C5 C4 C3
Number of rows = 4
D4 D3 D2 D1
D5 D4 D3 D2
D6 D5 D4 D3
D7 D6 D5 D4*/




import java.util.*;
class Mixed2{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter rows: ");
                int row = sc.nextInt();

		char ch = (char)(row+64);
		int num = row;

		for(int i = 1; i<=row; i++){
			for(int j = 1; j<=row; j++){
				System.out.print(ch+""+num-- +"\t");
			}
			num = i + row;
			System.out.println();
		}
	}
}

