



/*Rows = 3
c
3 2
c b a

Rows = 4
d
4 3
d c b
4 3 2 1*/


import java.util.*;
class Mixed6{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int row = sc.nextInt();

		for(int i = 1; i<=row; i++){
			int num = row;
			for(int j = 1; j<=i; j++){
				if(i%2==1){
					System.out.print((char)(num+96)+"\t");
				}
				else{
					System.out.print(num+"\t");
				}
				num--;
			}
			System.out.println();
		}
	}
}


