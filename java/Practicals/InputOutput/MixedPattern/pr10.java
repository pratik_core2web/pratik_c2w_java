


/*Q10 Write a program to print the square of odd digits from the given
number. (First reverse this number and then perform the operation)
Input : 45632985632
Output : 25, 9, 81, 25, 9*/



import java.util.*;
class Mixed10{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                int num = sc.nextInt();
                   
		int lastDigit = 0;
	        int reverse = 0;

                while(num!=0){
			lastDigit = num%10;
			reverse = reverse*10;
			reverse += lastDigit;
			num/=10;

		}
		System.out.println(reverse);

                int temp = reverse;
                while(temp!=0){
                       lastDigit = temp%10;
		       if(lastDigit%2==1){
			       System.out.print(lastDigit*lastDigit +"  ");
		       }
		       temp/=10;
		}
		System.out.println();
	}
}

