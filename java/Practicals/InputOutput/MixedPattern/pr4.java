
/*Q4 Write a program to check whether the given number is composite or
not.
Row = 3
3
2 4
1 2 3
Rows = 4
4
3 6
2 4 6
1 2 3 4*/


import java.util.*;

class Mixed4{

        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter rows: ");
                int row = sc.nextInt();
		
		for(int i = 1; i<=row; i++){		        
			int num = row+1-i;
			for(int j = 1; j<=i; j++){
				
				System.out.print(num +"\t"); 			         	 		
				num = num+ (row+1-i);
			}
			
			System.out.println();
		
		}
	}
}

