
/*9. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
D C B A
  D C B
    D C
      D
 
Rows = 5
E D C B A
  E D C B
    E D C
      E D
        E*/


import java.util.*;
class SpaceTriangle9{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("enter rows: ");
		int row = sc.nextInt();

		for(int i = 1; i<=row; i++){
			for(int spc = 1; spc < i; spc++){
				System.out.print("\t");
			}
                        char num = (char)(row+64);
			for(int j = 1; j<= row+1-i; j++){
				System.out.print(num-- +"\t");
			}
			System.out.println();
		}
	}
}


