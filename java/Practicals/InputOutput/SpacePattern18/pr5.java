/*5. WAP in notebook & Dry run first then type

Take number of rows from user :
Row = 3

    1
  2 4
3 6 9
Rows = 4

       1
     2 4
  3  6 9
4 8 12 16*/


import java.util.*;
class SpaceTriangle5{
        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);;
                System.out.print("Enter the Rows: ");
                int row = sc.nextInt();

		for(int i = 1; i<=row; i++){
			for(int space = 1; space<=row-i; space++){
				System.out.print("\t");
			}

		        int num = i;                                      
			for(int j = 1; j<=i; j++){
				System.out.print(num + "\t");				
			         num+=i;
			}
			System.out.println();
		}
	}
}

