/*2. WAP in notebook & Dry run first then type
Take number of rows from user:
Rows = 3
    3
  3 2
3 2 1

Rows = 4

      4
    4 3
  4 3 2
4 3 2 1*/


import java.util.*;
class SpaceTriangle2{
        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);;
                System.out.print("Enter the Rows: ");
                int row = sc.nextInt();

		for(int i = 1; i<=row; i++){
			for(int space = 1; space<=row-i; space++){
				System.out.print("\t");
			}
			int num = row;

			for(int j = 1; j<=i; j++){
				System.out.print(num-- + "\t");
			}
			System.out.println();
		}
	}
}

