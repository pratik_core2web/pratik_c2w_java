/*3. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
    C
  B C
A B C
Rows = 4
      D
    C D
  B C D   
A B C D*/

import java.util.*;
class SpaceTriangle3{
        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);;
                System.out.print("Enter the Rows: ");
                int row = sc.nextInt();

		for(int i = 1; i<=row; i++){
		        char ch = 65;
			for(int space = 1; space<=row-i; space++){
				System.out.print("\t");
				ch++;
			}			

			for(int j = 1; j<=i; j++){
				System.out.print(ch++ + "\t");
			}
			
			System.out.println();
		}
	}
}

