

/*10. WAP in notebook & Dry run first then type
Take number of rows from user:
rows = 4
65 B 67 D
   B 67 D
     67 D
        D

rows = 3
A 66 C
  66 C
     C*/

import java.util.*;
class SpaceTriangle10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("enter rows: ");
		int row = sc.nextInt();

		for(int i = 1; i<=row; i++){
			for(int spc = 1; spc < i; spc++){
				System.out.print("\t");
			}
                        int num = 64+i;	

			for(int j = 1; j<= row+1-i; j++){
				int sum = i+j;
				if(row%2==0){
					if(sum%2!=0){
						System.out.print((char)(num++) + "\t");
					}
					else{
						System.out.print(num++ +"\t");
					}
				}
				else{
					if(sum%2!=0){
						System.out.print(num++ +"\t");
					}
					else{
						System.out.print((char)num++ +"\t");
			
					}
				}
									       				
			}
			System.out.println();
		}
	}
}

