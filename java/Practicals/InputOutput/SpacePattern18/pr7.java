/*7. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
1 2 3 4
  1 2 3
    1 2
      1

Rows = 3
1 2 3
  1 2
    1*/


import java.util.*;
class SpaceTriangle7{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the rows: ");
		int row = sc.nextInt();

		for(int i = 1; i<=row; i++){
			for(int spc = 1; spc<i; spc++){
				System.out.print("\t");
			}

			for(int j = 1; j<=row+1-i; j++){
				System.out.print(j+"\t");
			}
			System.out.println();
		}
	}
}

