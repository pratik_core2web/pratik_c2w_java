/*8. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
1 2 3 4
  2 3 4
    3 4
      4

Rows = 3
1 2 3
  2 3
    3*/


import java.util.*;
class SpaceTriangle8{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("enter rows: ");
		int row = sc.nextInt();

		for(int i = 1; i<=row; i++){
			for(int spc = 1; spc < i; spc++){
				System.out.print("\t");
			}
                        int num = i;
			for(int j = 1; j<= row+1-i; j++){
				System.out.print(num++ +"\t");
			}
			System.out.println();
		}
	}
}


