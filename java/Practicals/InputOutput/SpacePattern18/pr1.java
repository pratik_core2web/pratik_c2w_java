/*1. WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3

    1
  1 2
1 2 3

Rows = 4

      1
    1 2
  1 2 3
1 2 3 4*/


import java.util.*;
class SpaceTriangle1{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);;
		System.out.print("Enter the Rows: ");
		int row = sc.nextInt();

		for(int i = 1; i<=row; i++){
			for(int space = 1; space<=row-i; space++){
				System.out.print("\t");
			}

			for(int j = 1; j<=i; j++){
				System.out.print(j+"\t");
			}
			System.out.println();
		}
	}
}


