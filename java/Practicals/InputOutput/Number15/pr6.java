/*Q6 Write a program to print the factorial of the number.
Input : 8
Output : Factorial of 8 is 40320.*/

import java.util.*;
class Number6 {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number : ");
		int num = sc.nextInt();
		int temp = num;

		long fact = 1;
		while(num>0){
			fact*=num;
			num--;
		}

		System.out.println("Factorial of  "+temp+ " is "+fact);
	}
}

			
