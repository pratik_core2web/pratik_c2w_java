/*Q5 Write a program to print the factorial of the number.
Input : 5
Output : Factorial of 5 is 120.*/

import java.io.*;

class Number5 {
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the Number: ");
		int num = Integer.parseInt(br.readLine());
                
		int prdt =1;
		int i = 1;

		while(i<=num){
			prdt = prdt * i;
			i++;	
		}

		System.out.println("Factorial is " +prdt);
	}
}






