/*Q9 Write a program to check whether the given number is palindrome or
not.
Input : 12121
Output : 12121 is a palindrome number.
Input : 12345
Output : 12345 is not a palindrome number.*/

import java.util.*;
class Number9 {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number : ");
		int num = sc.nextInt();

		int temp = num;
		int lastDigit = 0;
		int reverse =0;

		
		while(num>0){
			lastDigit = num%10;
			reverse = reverse*10 + lastDigit;
			num/=10;
		}

		System.out.println("Reverse of "+temp+" is "+reverse);

		if(temp == reverse)
			System.out.println(temp+" is a pallindrome number.");
		else
			System.out.println(temp+" is not a pallindrome number.");

	}
}

			
