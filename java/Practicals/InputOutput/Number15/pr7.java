/*Q7 Write a program to reverse the given number.
Input : 7853
Output : Reverse of 7853 is 3587.*/

import java.util.*;
class Number7 {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number : ");
		int num = sc.nextInt();

		int temp = num;
		int lastDigit = 0;
		int reverse =0;

		
		while(num>0){
			lastDigit = num%10;
			reverse = reverse*10;
			reverse = reverse + lastDigit;
			//reverse = lastDigit + reverse *10;
			num/=10;
		}
		System.out.println("Reverse of "+temp+" is "+reverse);
	}
}

			
