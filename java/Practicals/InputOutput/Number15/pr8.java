/*Q8 Write a program to reverse the given number.
Input : 7853
Output : Reverse of 7853 is 3587.*/

import java.util.*;
class Number8 {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number : ");
		int num = sc.nextInt();

		int temp = num;
		int lastDigit = 0;
		int reverse =0;

		
		while(num>0){
			lastDigit = num%10;
			reverse = reverse*10 + lastDigit;
			num/=10;
		}
		System.out.println("Reverse of "+temp+" is "+reverse);
	}
}

			
