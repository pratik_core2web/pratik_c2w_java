/*Q3 Write a program to check whether the given number is composite or
not.
Input: 6
Output: 6 is a composite number.
Input: 11
Output: 11 is not a composite number.*/

import java.io.*;

class Number3 {
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the Number: ");
		int num = Integer.parseInt(br.readLine());

		int count =0;
                int i = 1;
		while(i<=num/2){
			if(num%i==0){
				count++;
			}
			i++;
		}

		if(count > 1)
			System.out.println(num+ " is a composit number..." );
		else
			System.out.println(num+ " is a not a composit number..." );
	}
}






