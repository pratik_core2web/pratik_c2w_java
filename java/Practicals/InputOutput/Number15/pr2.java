/*Q1 Write a program to check whether the given number is prime or not.
Input: 7
Output: 7 is a prime number.
Input: 12
Output: 12 is not a prime number.*/

import java.io.*;

class Number2{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the Number: ");
		int num = Integer.parseInt(br.readLine());



		int count =0;
                int i = 1;
		while(i<=num/2){
			if(num%i==0){
				count++;
			}
			i++;
		}

		if(count ==1)
			System.out.println(num+ " is a prime number..." );
		else
			System.out.println(num+ " is a not a prime number..." );
	}
}






