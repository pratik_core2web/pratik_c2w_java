/*7. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
A 4 A
6 B 8
C 10 C

row=4
4 A 6 A
8 B 10 B
12 C 14 C
16 D 18 D*/

import java.io.*;
class Sp7{
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());
                
		int num = row;
		for(int i = 1; i<=row; i++){
			
			for(int j = 1; j<=row; j++){

				if(num%2==0)
					System.out.print(num +"\t");
				else
					System.out.print((char)(i+64)+ "\t");

				num++;
			}
			System.out.println();
		}
	}
}

