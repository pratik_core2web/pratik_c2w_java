/*1. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
C B A
3 3 3
C B A
row=4
D C B A
4 4 4 4
D C B A
4 4 4 4*/

import java.io.*;
class Sp1{
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<=row; i++){
			int num = row +64;
			for(int j = 1; j<=row; j++){

				if(i%2==1)
					System.out.print((char)(num--) +"\t");

				else
					System.out.print(row +"\t");
			}
			System.out.println();
		}
	}
}

