/*9. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
2 6 6
3 4 9
2 6 6
row=4
2 6 6 12
3 4 9 8
2 6 6 12
3 4 9 8*/

import java.io.*;
class Sp9 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());
             
		for(int i = 1; i<=row; i++){
			int num = 1;
			for(int j = 1; j<=row; j++){
                                
				if(i%2==1){
					if(j%2==1)
		         			System.out.print(num*2 +"\t");
					else
		         			System.out.print(num*3 +"\t");
				}

				else{
					if(j%2==1)
		         			System.out.print(num*3 +"\t");
					else
		         			System.out.print(num*2 +"\t");
				}
				num++;
			}
			System.out.println();
		}
	}
}

