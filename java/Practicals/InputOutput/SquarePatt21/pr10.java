/*10. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
3 B 1
C B A
3 B 1
row=4
4 C 2 A
D C B A
4 C 2 A
D C B A*/

import java.io.*;
class Sp10 {
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());
             
		for(int i = 1; i<=row; i++){
			int num = row;
			for(int j = 1; j<=row; j++){

				if(i%2==1){
					if(j%2==1)
						System.out.print(num+ "\t");

					else
						System.out.print((char)(num+64)+ "\t");
				}
                              
				else{
					System.out.print((char)(num+64)+ "\t");
				}

				num--;
			}
			System.out.println();
		}
	}
}

