/*5. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
3 16 5
36 7 64
9 100 11 

row=4
16 5 36 7
64 9 100 11
144 13 196 15
256 17 324 19*/

import java.io.*;
class Sp5{
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());
                
		int num = row;
		for(int i = 1; i<=row; i++){
			
			for(int j = 1; j<=row; j++){

				if(num%2==0)
					System.out.print(num*num +"\t");
				else
					System.out.print(num+ "\t");

				num++;
			}
			System.out.println();
		}
	}
}

