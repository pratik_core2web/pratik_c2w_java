/*2. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
9 4 25
18 7 8
27 50 11

row=4
4 25 18 7
8 27 50 11
36 13 14 45
16 17 54 19*/

import java.io.*;
class Sp2{
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());
                
		int num = row;
		for(int i = 1; i<=row; i++){
			
			for(int j = 1; j<=row; j++){

				if(num%3==0)
					System.out.print(num*3 +"\t");

				else if(num%5==0)
					System.out.print(num*5 +"\t");

				else
					System.out.print(num+ "\t");

				num++;
			}
			System.out.println();
		}
	}
}

