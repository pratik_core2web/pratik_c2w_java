/*4. WAP in notebook & Dry run first then type
Take number of rows from user :
row=3
C 4 5
F 7 8
I 10 11

row=4
D 5 6 7
H 9 10 11
L 13 14 15
P 17 18 19*/

import java.io.*;
class Sp4{
	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());
                
		int num = row;
		for(int i = 1; i<=row; i++){
			
			for(int j = 1; j<=row; j++){

				if(j==1)
					System.out.print((char)(num+64) +"\t");
				else
					System.out.print(num+ "\t");

				num++;
			}
			System.out.println();
		}
	}
}

