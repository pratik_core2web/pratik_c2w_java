/*Write a program to find the number of occurrences of a specific number in an array. Print
the count of occurrences.
2 5 2 7 8 9 2
Input Specific number: 2
Output : Number 2 occurred 3 times in an array.
Input: Specific number : 11
Output: num 11 not found in array.*/

import java.util.*;
class Array3 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size: ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.print("Enter the elements: ");

		for(int i = 0; i<size; i++){
			arr[i]=sc.nextInt();
		}

	 	System.out.print("Enter Specific element");
		int num = sc.nextInt();
		int count = 0;

		for(int i =0; i<size; i++){
			if(arr[i]==num){
				count++;
			}
		}
		if(count > 0){
			
			System.out.println("Number "+num+ " Occured " +count+ " times in an array.");
		}

		else{
			System.out.println("Number "  +num+ " not found");
		}



	}

}
		


