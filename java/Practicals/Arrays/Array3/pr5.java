/*Q5
Write a program to convert all negative numbers into their squares in a given array.

-2 5 -6 7 -3 8

Output:
4 5 36 7 9 8*/


import java.util.*;
class Array5 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size: ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.print("Enter the elements: ");

		for(int i = 0; i<size; i++){
			arr[i]=sc.nextInt();
		}

		for(int i = 0; i<size; i++){
                        if(arr[i]<0){
				arr[i]= arr[i]*arr[i];
			}
		}

		for(int i = 0; i<size; i++){
			System.out.print(arr[i]+"\t");
		}

		System.out.println();
	}

}
		


