/*Q10.
Write a program to print the product of prime elements in an array.
1 4 5 6 11 9 10
Output:*/


import java.util.*;
class Array10 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size: ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.print("Enter the elements: ");

		for(int i = 0; i<size; i++){
			arr[i]=sc.nextInt();
		}
                
		int prdt =1;
		int count =0;
		for(int i = 0; i<size; i++){
			count=0;
			for (int j = 1; j<=arr[i]/2; j++){
				if(arr[i]%j==0){
					count++;
				}
			}
			if(count==1){
				prdt*=arr[i];
			}
		}
		System.out.println(prdt+"  is the product of prime no's in an array... ");
	}

}
		
