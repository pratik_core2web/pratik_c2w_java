/*Q9.
Print the composite numbers in an array.
5 7 9 11 15 19 90
Output:
5 7 11 19*/


import java.util.*;
class Array8 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size: ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.print("Enter the elements: ");

		for(int i = 0; i<size; i++){
			arr[i]=sc.nextInt();
		}
                
		int count =0;
		for(int i = 0; i<size; i++){
			count=0;
			for (int j = 1; j<=arr[i]/2; j++){
				if(arr[i]%j==0){
					count++;
				}
			}
			if(count>1)	
				System.out.print(arr[i]+"\t");
		}
		System.out.println();
	}

}
		


