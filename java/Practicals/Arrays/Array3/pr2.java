/*Q2
Write a program to find the first occurrences of a specific number in an array. Print the
index of a first occurrence.
Example :
1 5 9 8 7 6
Input: Specific number : 5
Output: num 5 first occurred at index : 1
Input: Specific number : 11
Output: num 11 not found in array.*/

import java.util.*;
class Array2 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size: ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.print("Enter the elements: ");

		for(int i = 0; i<size; i++){
			arr[i]=sc.nextInt();
		}

		System.out.print("Enter specific element: ");
	        int num = sc.nextInt();
 		int flag = 0;

		for(int i = 0; i<size; i++){
			flag = 0;
			if(arr[i]==num){
				System.out.println("Number" + num+ " first occured at "+i);
				break;
			}
			else {
			   	flag =1;	
			}
		}

		if (flag ==1)
			System.out.println("Number not found..");


	}

}
		


