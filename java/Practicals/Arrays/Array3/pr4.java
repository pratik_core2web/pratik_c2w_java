/*Q4
Write a program to convert all even numbers into 0 and odd numbers into 1 in a given
array.
1 2 3 12 15 6 7 10 9
Output:

1 0 1 0 1 0 1 0 1*/


import java.util.*;
class Array4 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size: ");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.print("Enter the elements: ");

		for(int i = 0; i<size; i++){
			arr[i]=sc.nextInt();
		}

		for(int i = 0; i<size; i++){
                        if(arr[i]%2==0){
				arr[i]=0;
			}
			else{
				arr[i]=1;
			}

		}
		
		for(int i = 0; i<size; i++){
			System.out.print(arr[i]+"\t");
		}
		System.out.println();
	}

}
		


