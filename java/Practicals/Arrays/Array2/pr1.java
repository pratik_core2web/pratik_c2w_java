/*1.WAP to count the even numbers in an array where you have to take the size and
elements from the user. And also print the even numbers too
Example:
Enter size =8
1 12 55 65 44 22 36 10
Output : even numbers 12 44 22 36 10
Count of even elements is : 5*/

import java.util.*;
class Array1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of an array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.print("Enter array elements : ");

		for(int i = 0; i<size; i++){
			arr[i]=sc.nextInt();
		}
                
		int cnt = 0;
		System.out.print("Even numbers : ");
		for(int i = 0; i<size; i++){
			if(arr[i]%2==0){
				cnt++;
				System.out.print(arr[i]+"  ");
			}
		}
		System.out.println("\nCount of even elements: "+cnt);
	}
}


