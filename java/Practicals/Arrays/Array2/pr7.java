/*7.WAP to print the array , if the user given size of an array is even then print the alternate
elements in an array, else print the whole array.
Example 1:
Input:
Enter the size
5
Enter elements:
12 3 4 5
Output:
Array elements are:
1 2 3 4 5
Example 2:
Input:
Enter the size
4
Enter elements:
1 2 3 4
Output:
Array elements are:
1 3*/


import java.util.*;
class Array7 {
	public static void main(String args[]){
		Scanner sc = new  Scanner(System.in);
		System.out.print("Enter the size: ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.print("Enter the elements : ");
		for(int i = 0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}

		int num =0;
	        if(size %2==0){
			for(int i = 0; i<arr.length/2; i++){
				System.out.print(arr[num]+"\t");
				num+=2;
			}
		}

		else{
			for(int i = 0; i<arr.length;i++){
				System.out.print(arr[i]+"\t");
			}
		}
		System.out.println();	
	}
}

