/*6.Write a program to print the products of odd indexed elements in an array. Where you
have to take size input and elements input from the user.
Note:
Example:
Input:
Enter the size
6
Enter elements:
1 2 3 4 5 6
Output:
product of odd indexed elements : 48*/
import java.util.*;
class Array6{
	public static void main(String args[]){
		Scanner sc = new  Scanner(System.in);
		System.out.print("Enter the size: ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.print("Enter the elements : ");
		for(int i = 0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}

		int prdt = 1;

		for(int i = 0; i<arr.length; i++){
			if(i%2==1){
				prdt *= arr[i];
			}
		}

		System.out.println("Product of odd indexed elements : "+prdt);
	}
}
		
