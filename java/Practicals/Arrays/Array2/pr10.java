/*10. WAP to print the maximum element in the array.*/


import java.util.*;
class Array10 {
	public static void main(String args[]){
		Scanner sc = new  Scanner(System.in);
		System.out.print("Enter the size: ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		System.out.print("Enter the elements : ");
		for(int i = 0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}
                
		int max = arr[0];
		for(int i = 1; i<arr.length; i++){
			if(arr[i] > max){
				max = arr[i];
			}
		}

		System.out.println("Minimum element in array is "+max);
	}
}
		
