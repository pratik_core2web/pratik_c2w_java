/*3.WAP to check if there is any vowel in the array of characters if present then print its
index, where you have to take the size and elements from the user.
Example:
Input:
Enter the size
5
Enter elements:
arEKO
Output:
vowel a found at index 0
vowel E found at index 2
vowel O found at index*/

import java.util.*;
class Array3 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of an array: ");
		int size = sc.nextInt();

		char arr[] = new char[size];

		System.out.print("Enter array elements : ");

		for(int i = 0; i<size; i++){
			arr[i]=sc.next().charAt(0);
		}
                
		for(int i = 0; i<size; i++){
			if(arr[i]=='A'||arr[i]=='E'||arr[i]=='I'||arr[i]=='O'||arr[i]=='U'){
				System.out.println("Vowel "+arr[i]+" Found at index "+i);
			}
			else if(arr[i]=='a'||arr[i]=='e'||arr[i]=='i'||arr[i]=='o'||arr[i]=='u'){
				System.out.println("Vowel "+arr[i]+" Found at index "+i);
		        }
		}
	}
}


