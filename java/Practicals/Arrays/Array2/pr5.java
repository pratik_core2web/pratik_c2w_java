/*5.Write a program to print the sum of odd indexed elements, in an array. Where you have
to take size input and elements input from the user .
Example:
Input:
Enter the size
5
Enter elements:
4 8 2 6 7
Output:
Sum of odd indexed elements : 14*/

import java.util.*;
class Array5 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of an array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.print("Enter array elements : ");

		for(int i = 0; i<size; i++){
			arr[i]=sc.nextInt();
		}
                
		int sum = 0;
		for(int i = 0; i < size; i++){
			if(i % 2 == 1){
				sum += arr[i];
			}
		}
		
		System.out.println("Sum of odd indexed elements in an array : "+sum);
	}
}


