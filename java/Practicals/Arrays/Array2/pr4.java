/*4.WAP to search a specific element in an array and return its index. Ask the user to
provide the number to search, also take size and elements input from the user.
Example:
Input:
Enter the size
5
Enter elements:
12 144 13 156 8
Enter the number to search in array:
8

Output:
8 found at index 4*/

import java.util.*;
class Array4 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of an array: ");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.print("Enter array elements : ");

		for(int i = 0; i<size; i++){
			arr[i]=sc.nextInt();
		}

		System.out.print("Enter number to search : ");
                int num = sc.nextInt();
                int temp = 0;		
		for(int i = 0; i<size; i++){
			temp=arr[i];
			if(temp==num){
				System.out.println(arr[i]+" found at "+i);
                            }
		}
		

	}
}


