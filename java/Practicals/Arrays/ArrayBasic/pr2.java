/*2. Take an input from the user where the size of the array should be 10 and print the
output of the user given elements of an array.*/


import java.io.*;

class Array2{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter size of array: ");
		int size = Integer.parseInt(br.readLine());

		int arr1[] = new int[size];

		for(int i = 0; i<arr1.length; i++){
			System.out.print("Enter array element " +i+" : ");
			arr1[i]= Integer.parseInt(br.readLine());
		}

		for(int i = 0; i<arr1.length; i++){
			System.out.print(arr1[i]+"   ");
		}
		System.out.println();

	}
}


