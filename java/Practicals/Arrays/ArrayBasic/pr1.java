/*1. Write a program to print the array with minimum 10 elements data.
Example:
Array:
10 20 30 40 50 60 70 80 90 100
Output :
10, 20, 30, 40, 50, 60, 70, 80, 90, 100*/


class Array1{
	public static void main(String[] args){
		int arr1[] = new int[10];

		int num = 10;

		for(int i = 0; i<arr1.length; i++){
			arr1[i]= num;
			System.out.print(arr1[i]+ "  ");
			num+=10;
		}
		System.out.println();
	}
}
