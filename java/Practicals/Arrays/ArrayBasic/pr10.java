/*9. Write a program where you have to print the even indexed elements. Take input from
the user
Example :
Enter size: 10.
1 2 3 4 5 6 7 8 9 10

1  3  5  7  9*/


import java.util.*;

class Array10{
	public static void main(String[] args){
		Scanner sc= new Scanner(System.in);
		System.out.print("Enter array size: ");
		int size = sc.nextInt();
		int arr[] = new int[size];

		for(int i = 0; i<arr.length; i++){
			System.out.print("enter elements: ");
			arr[i]= sc.nextInt();
		}

		for(int i = 0; i<arr.length; i++){
			if(i % 2 == 0){
			System.out.println(arr[i]+" is an even indexed element.");
			}
		}
		
	}
}




