/*4. Write a program to print the sum of odd elements in an array.Take input from the user.
Example:
Enter size: 10
Array:
1 2 3 4 2 5 6 2 8 10
Output :
Sum of odd elements : 9*/


import java.util.*;
class Array4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size: ");
		int size = sc.nextInt();

		int arr[]= new int[size];
                int sum = 0;
		for(int i = 0 ; i<arr.length; i++){
		         System.out.print("Enter elements: ");
			 arr[i]= sc.nextInt();

			 if(arr[i] % 2 !=0)
			 sum+=arr[i];

		}
		System.out.println(sum);

			
	}
}
