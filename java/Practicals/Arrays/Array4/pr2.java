/*Q2.WAP to find the difference between minimum element in an array and maximum
element in an array, take input from the user.
Example :
Input:
Enter the size :
5
Enter the elements of the array:
3 6 9 8 10
Output
The difference between the minimum and maximum elements is: 7*/
import java .util.*;
class Array2 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size : ");
		int size = sc.nextInt();

		int arr[] = new int [size];

		for(int i = 0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}

		int max = arr[0];
		for(int i = 1; i<arr.length; i++){
			if(arr[i]>max){
				max=arr[i];
			}
		}
		System.out.println("Maximum element is "+max);

		int min = arr[0];
		for(int i = 1; i<arr.length; i++){
			if(arr[i]<min){
				min=arr[i];
			}
		}
		System.out.println("Minimum element is "+min);

		System.out.println((max-min)+"  is the difference between maximum and minimum element.");
	}
}

		
