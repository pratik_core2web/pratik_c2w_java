/*Q6. WAP to count the vowels and consonants in the given array(Take input from the user)
Example:
Enter the size of the array:
6
Enter the elements of the array:
a
E
P
o
U
G
Output:
Count of vowels: 4

Count of consonants: 2*/
import java .util.*;
class Main {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Size of Array: ");
		int size = sc.nextInt();

		char arr[] = new char[size];

                System.out.println("Enter arraay elements: ");
		for(int i =0; i<arr.length; i++){
			arr[i] = sc.next().charAt(0);
		}
                
		int vwlCount=0;
		int cnsntCount=0;
                
		for(int i =0; i<arr.length; i++){
			if(arr[i]=='a'|| arr[i]=='e'|| arr[i]=='i'||arr[i]=='o'||arr[i]=='u' ){
				vwlCount++;
			}
			else if(arr[i]=='A'|| arr[i]=='E'|| arr[i]=='I'||arr[i]=='O'||arr[i]=='U' ){
				vwlCount++;
			}
			else{
				cnsntCount++;
			}
		}
		System.out.println("Count of Vowel: "+vwlCount);
		System.out.println("Count of consonant: "+cnsntCount);
		
	}
}


