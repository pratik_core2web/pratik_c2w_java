/*Q10.Write a program to print the characters in an array which comes before the user
given character.
Output:
Input:
Enter the size:
6
Enter Elements
A
B
Y
G
H
P
Enter character key :
H
Output:
Array:
A
B
Y
G*/


import java .util.*;
class Main5 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Size of Array: ");
		int size = sc.nextInt();

		char arr[] = new char[size];
                System.out.println("Enter arraay elements: ");

		for(int i =0; i<arr.length; i++){
			arr[i] = sc.next().charAt(0);
		}
                
		System.out.print("enter character key: ");
		char key = sc.next().charAt(0);

	        int i = 0;
		while(arr[i]!=key){
			System.out.print(arr[i]+"\t");
			if(arr[i]==key){
				break;
			}
			i++;
		}
		System.out.println();
		
	}
}


