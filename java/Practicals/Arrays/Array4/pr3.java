/*Q3. WAP to find the second largest element in an array.
Example:
Input:
Enter the size :4
Enter the elements of the array:
1 2 3 4
Output:
The second largest element in the array is: 3*/


import java .util.*;
class Array3 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size : ");
		int size = sc.nextInt();

		int arr[] = new int [size];

		System.out.print("Enter the elements : ");
		for(int i = 0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}

		int max = arr[0];
		for(int i = 1; i<arr.length; i++){
			if(arr[i]>max){
				max=arr[i];
			}
		}
		System.out.println("Maximum element is "+max);

		int smax = arr[0];
		for(int i = 1; i<arr.length; i++){
			if(arr[i]>=smax && arr[i]<max){
				smax=arr[i];
			}
		}
		System.out.println("Seceond Maximum element is "+smax);
	        
	}
}

		
