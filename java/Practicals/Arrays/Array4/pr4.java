/*Q4. WAP to check whether the user given number occurs more than 2 times or equals 2
times.
Example :

Input:
Enter the size of the array:
6
Enter the elements of the array:
56 65 78 56 90 56
Enter the number to check:
56
Output:
56 occurs more than 2 times in the array.*/



import java .util.*;
class Array4 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size : ");
		int size = sc.nextInt();

		int arr[] = new int [size];

		System.out.print("Enter the elements : ");
		for(int i = 0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}

		System.out.print("Enter the specific number : ");
		int num = sc.nextInt();
		int count = 0;

		for(int i = 1; i<arr.length; i++){
			if(arr[i]==num){
			 	count++;
			}
	      }
	      if(count==2)
		      System.out.println("Number occured 2 times");
	      else if(count<2)
		      System.out.println("Number occured less than 2 times");
	      else
		      System.out.println("Number occured more than 2 times");
	
	        
	}
}

		
