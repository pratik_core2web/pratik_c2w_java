/*Q7. WAP to convert lowercase characters to UPPERCASE characters.(Take input from
the user)
Example:
Input:
Enter the size of the array:
6
Enter the elements of the array:
a
B
Y
p
o
H
Output:
A B Y P O H*/
import java .util.*;
class Main2 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Size of Array: ");
		int size = sc.nextInt();

		char arr[] = new char[size];
                System.out.println("Enter arraay elements: ");

		for(int i =0; i<arr.length; i++){
			arr[i] = sc.next().charAt(0);
		}
                
		for(int i =0; i<arr.length; i++){
			if(arr[i] >= 97 && arr[i] <= 122){
				arr[i] -= 32;
			}
			System.out.print(arr[i]+"\t");
		}
		System.out.println();
		
	}
}


