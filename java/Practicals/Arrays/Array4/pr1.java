/*Q1.WAP to take input from the user for size and elements of an array, where you have to
print the average of array elements(Array should be of integers).
Example :
Input:
Enter the size:
4
Enter array elements:
2 4 6 8
Output:
Array elements' average is :5*/

import java .util.*;
class Array1 {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size : ");
		int size = sc.nextInt();

		int arr[] = new int [size];

		for(int i = 0; i<arr.length; i++){
			arr[i]=sc.nextInt();
		}

		int sum =0;
		System.out.print("Enter the elements : ");
		for(int i = 0; i<arr.length; i++){
			sum+=arr[i];	
		}

		System.out.println(sum/size +"  is the average of array elements.");
	}
}

		
