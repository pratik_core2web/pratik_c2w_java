



class EvenOddPrdtSum{
	public static void main(String[] args){
		int num = 9367924;
		int lastDigit = 0;
		int sum = 0;
		int prdt = 1;

		while(num!=0){
			lastDigit = num % 10;

			if(lastDigit % 2 != 0){
				sum += lastDigit;
			}

			else{
				prdt *= lastDigit;
			}

			num /= 10;
		}

		System.out.println("Sum of Odd digits : " +sum);
		System.out.println("Product of even digits : " + prdt);
	}
}

