


class OddSqreSum{
	public static void main(String[] args){

		int num = 2469185;
		int lastDigit = 0;
		int square = 0;
		int sumOfSqrs = 0;

		System.out.print("Sum of squares of odd digits : ");

		while(num != 0){
			lastDigit = num% 10;

			if(lastDigit % 2 != 0){
				square = lastDigit * lastDigit;
				sumOfSqrs += square;
			}

			num/=10;
		}

		System.out.println(sumOfSqrs);
	}
}


