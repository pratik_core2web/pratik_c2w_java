



class Product{
	public static void main(String [] args){
		int num = 234;
		int lastDigit = 0;
		int prdt = 1;

		while(num!=0){

			lastDigit = num% 10;
			prdt *= lastDigit;
			
			num /= 10;
		}

		System.out.println(prdt);
	}
}

