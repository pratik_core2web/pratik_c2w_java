 



class OddSquare{
	public static void main(String[] args){
		int num = 256985;
		int lastDigit = 0;

		System.out.print("Square of odd digits are  : ");

		while(num!=0){
			lastDigit = num % 10;

			if(lastDigit % 2 != 0){

		        System.out.print(lastDigit * lastDigit+ "  ");
			}

			num/=10;
		}


		System.out.println();
	}
}

