 



class EvenCubes{
	public static void main(String[] args){
		int num = 216985;
		int lastDigit = 0;

		System.out.print("Cubes of even digits are  : ");

		while(num!=0){
			lastDigit = num % 10;

			if(lastDigit % 2 == 0){

		        System.out.print(lastDigit * lastDigit * lastDigit+ "  ");
			}

			num/=10;
		}


		System.out.println();
	}
}

