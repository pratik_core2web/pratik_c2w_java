/*10.Write a program to print the sum of digits in the given number.
Input: 9307922405
Output: sum of digits in 9307922405 is 41*/


class SumOfDigits{
	public static void main(String[] args){

		long num = 9307922405l;
		long lastDigit = 0;
		long sum = 0;

		while(num!=0){
			lastDigit = num % 10;
			sum += lastDigit;

			num/=10;
		}

		System.out.println(sum);
	}
}


