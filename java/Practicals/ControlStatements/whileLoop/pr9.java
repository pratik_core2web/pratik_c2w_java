/*Write a program to count the odd digits and even digits in the given
number.
Input: 214367689
Output: Odd count : 4
Even count : 5*/


class EvenOdd{
	public static void main(String[] args){

		int num = 214367689;
		int evenCount = 0;
		int oddCount = 0;
		int lastDigit = 0;

		while(num>0){
			lastDigit = num%10;
                        
			if(lastDigit % 2 != 0)
			        oddCount++;

			else
				evenCount++;
                        
			num/=10;

		}

		System.out.println(oddCount);
		System.out.println(evenCount);

	}
}


