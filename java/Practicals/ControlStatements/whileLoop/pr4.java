/*Write a program to print the character sequence given below where the
input: start=1, end =6
This means iterate the loop from 1 to 6
Output: A 2 C 4 E 6 (if num is odd print the character)*/

class Loop4{
	public static void main(String[] args){

		char ch = 'A';

		int i = 1;
                while(i<=6){

			if(i%2 != 0){
				System.out.print(ch+"  ");
			}

			else{
				System.out.print(i+"  ");
			}

			ch++;
			i++;
		}

		System.out.println();
	}
}


