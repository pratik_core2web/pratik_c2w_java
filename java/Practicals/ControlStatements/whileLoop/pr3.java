/*Write a program to print the character sequence given below when the
range given is
Input : start = 1 and end =6.
This means iterate the loop from 1 to 6
Output: A B C D E F*/



class Loop3{
	public static void main(String[] args){

		char ch = 'A';	
		while(ch<='F'){
                        
	              System.out.print(ch+ "  ");
			
			ch++;
		}
		
		System.out.println();
	}
}



