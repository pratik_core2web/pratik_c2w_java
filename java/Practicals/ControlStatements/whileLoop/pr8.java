/*Write a program to print the odd digits of a given number.
Input : 216985
Output : 5 9 1*/

class OddNum{
	public static void main(String[] args){

		int num = 216985;
		int lastDigit = 0;

		while(num!=0){
			lastDigit = num%10;
			if(lastDigit % 2 != 0){
				System.out.print(lastDigit+ "  ");
			}

			num/=10;
		}		
		System.out.println();
	}
}

