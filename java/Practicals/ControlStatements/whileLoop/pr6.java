/*Write a program to print each digit on a new line of a given number
using a while loop
Input: num = 9307
Output: 
 7
 0
 3
 9*/



class WhileLoop1{

	public static void main(String[] args){

		int num = 9307;
		int lastDigit = 0;

		while(num>0){
			lastDigit = num%10;
			System.out.println(lastDigit+ "  ");
			num/=10;
		}
	}
}

