
class ifElse8{

        public static void main(String[] args){

                float percent = 88.09f;

                if(percent>=85.00){
                        System.out.println("passed with 1st class ditinction");
                }

                else if(percent>=75.00 && percent<=84.99){
                        System.out.println("passed with 1st class");
                }
                else if(percent>=65.00 && percent<=74.99){
                        System.out.println("passed in 2nd class");
                }
                else if(percent>=55.00 && percent<=64.99){
                        System.out.println("passed in 3rd class");
                }
                else if(percent>=45.00 && percent<=54.99){
                        System.out.println("just passed");
                }

                else{
                        System.out.println("Fail");
		}
	}
}

