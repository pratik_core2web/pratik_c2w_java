/*
 * WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
A
B C
C D E
D E F G*/


import java.util.Scanner;

class Triangle4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows ; ");
		int row = sc.nextInt();

		char ch = 'A';

		for(int i = 1; i<=row; i++){
			ch =(char)(i+64);
			for(int j = 1; j<=i; j++){
				System.out.print(ch++ +"  ");
			}
								     			 
			System.out.println();
		}
	}
}


