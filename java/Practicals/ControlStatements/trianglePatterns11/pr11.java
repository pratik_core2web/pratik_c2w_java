/*WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 4
D C B A
D C B
D C
D*/

import java.io.*;
class Triangle11{
	public static void main(String[] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows: ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<=row; i++){
			char ch = (char)(row+64);
			for(int j = 1; j <= row+1-i; j++){
				System.out.print(ch-- + "  ");
			}
			System.out.println();
		}
	}
}

