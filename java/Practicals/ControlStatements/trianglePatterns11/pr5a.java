/*WAP in notebook & Dry run first then type
Take number of rows from user :
Row = 3
3
3 6
3 6 9*/



import java.io.*;

class Triangle5{
        public static void main(String [] args)throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter the rows : ");
                int row = Integer.parseInt(br.readLine());

                for(int i = 1; i<=row; i++){
                        for(int j = 1; j<=i; j++){
                                System.out.print(j*3 + "  ");
                        }
                System.out.println();
                }
        }
}
