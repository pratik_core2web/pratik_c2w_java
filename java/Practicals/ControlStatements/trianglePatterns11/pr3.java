/*WAP in notebook & Dry run first then type
Take number of rows from user :
Rows = 3
3
3 2
3 2 1*/


import java.io.*;

class Triangle3{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter the rows : ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i<=row; i++){
			int temp = row;
			for(int j = 1; j<=i; j++){
				System.out.print(temp+"   ");
				temp--;
			}
		System.out.println();
		}
	}
}

